<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GoMallz</title>
    @includeIf('frontend.include.styles')
</head>
<body class="common-home ltr layout-6">
    <div id="wrapper" class="wrapper-full banners-effect-7">
{{--        @includeIf('frontend.include.loader')--}}
        @includeIf('frontend.include.header')
        <div id="content" class="">
            <div class="custom-scoll hidden-sm hidden-md hidden-xs">
                <div class="custom-html">
                    <div class="scoll-cate list_diemneo">
                        <ul id="nav-scroll">
                            <li class="neo1"><a href="#box-link1"><span>Hot deal</span></a></li>
                            <li class="neo2"><a href="#box-link2"><span>Spa</span></a></li>
                            <li class="neo3"><a href="#box-link3"><span>Fashion</span></a></li>
                            <li class="neo4"><a href="#box-link4"><span>Travel</span></a></li>
                            <li class="neo5"><a href="#box-link5"><span>Digital</span></a></li>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="so-page-builder">
                @includeIf('frontend.include.page-builder-ltr')
                @includeIf('frontend.include.section_1_h2')
            </div>
        </div>
        @includeIf('frontend.include.footer')
    </div>

    @includeIf('frontend.include.scripts')
</body>
</html>
