<section id="" class="section_1_h2 ">
    <div class="container page-builder-ltr">
        <div class="row row_o7yw  row-style ">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col_q4w9  menu_vertical">

                <div class="row row_u7kh  row-style ">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_1x14">
                        <div class="qr-code-block block">
                            <div class="item-image"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/qr.png"
                                    alt="" width="65" height="65"></div>
                            <div class="item-content">
                                <h4>Enjoy Convenient Order Tracking</h4>
                                <p><a href="#">Scan to download app</a></p>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_35kw block">

                        <div class="module so-extraslider-ltr home3_extra bn-shadow">

                            <h3 class="modtitle">Special Items</h3>


                            <div class="modcontent">


                                <div id="so_extra_slider_3849357241573891478"
                                     class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-1 preset04-1 button-type1">
                                    <div class="box-banner">
                                        <div class="banners">
                                        </div>
                                    </div>
                                    <!-- Begin extraslider-inner -->
                                    <div class="extraslider-inner products-list grid" data-effect="none">

                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='98'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=98"
                                                               target="_blank"
                                                               title="Pariatur beef desktop publishing package..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/5-270x270.jpg"
                                                                     alt="Pariatur beef desktop publishing packages monst">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=98"
                                                                   target="_blank"
                                                                   title="Pariatur beef desktop publishing packages monst ">
                                                                    Pariatur beef desktop publishing
                                                                    package..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$56.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$62.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='29'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                               target="_blank"
                                                               title="Est Officia Including Shoes Beautiful Pi..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/15-270x270.png"
                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                   target="_blank"
                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz ">
                                                                    Est Officia Including Shoes Beautiful
                                                                    Pi..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$337.99 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='42'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                               target="_blank"
                                                               title="Est Officia Including Shoes Beautiful Pi..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/24-270x270.png"
                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                   target="_blank"
                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz ">
                                                                    Est Officia Including Shoes Beautiful
                                                                    Pi..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='82'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=82"
                                                               target="_blank"
                                                               title="Maciti Aliqua occur that pleasures have ..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/21-270x270.png"
                                                                     alt="Maciti Aliqua occur that pleasures have a lotem">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=82"
                                                                   target="_blank"
                                                                   title="Maciti Aliqua occur that pleasures have a lotem ">
                                                                    Maciti Aliqua occur that pleasures have
                                                                    ..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='28'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=28"
                                                               target="_blank"
                                                               title="Amazing Yoga Sport Poses Most  People Wo..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/10-270x270.jpg"
                                                                     alt="Amazing Yoga Sport Poses Most  People Wouldn't Dream ">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=28"
                                                                   target="_blank"
                                                                   title="Amazing Yoga Sport Poses Most  People Wouldn't Dream  ">
                                                                    Amazing Yoga Sport Poses Most People
                                                                    Wo..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='57'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=57"
                                                               target="_blank"
                                                               title="Men Winter Down Coat Jackets  Women Thic..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/10-270x270.jpg"
                                                                     alt="Men Winter Down Coat Jackets  Women Thicken">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=57"
                                                                   target="_blank"
                                                                   title="Men Winter Down Coat Jackets  Women Thicken ">
                                                                    Men Winter Down Coat Jackets Women
                                                                    Thic..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                    </div>
                                    <!--End extraslider-inner -->

                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            (function (element) {
                                                var $element = $(element),
                                                    $extraslider = $(".extraslider-inner", $element),
                                                    _delay = 500,
                                                    _duration = 800,
                                                    _effect = 'none ';

                                                $extraslider.on("initialized.owl.carousel2", function () {
                                                    var $item_active = $(".owl2-item.active", $element);
                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {
                                                        var $item = $(".owl2-item", $element);
                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });
                                                    }


                                                    $(".owl2-controls", $element).insertBefore($extraslider);
                                                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                                });

                                                $extraslider.owlCarousel2({
                                                    rtl: false,
                                                    margin: 0,
                                                    slideBy: 1,
                                                    autoplay: 0,
                                                    autoplayHoverPause: 0,
                                                    autoplayTimeout: 0,
                                                    autoplaySpeed: 1000,
                                                    startPosition: 0,
                                                    mouseDrag: 1,
                                                    touchDrag: 1,
                                                    autoWidth: false,
                                                    responsive: {
                                                        0: {items: 1},
                                                        480: {items: 1},
                                                        768: {items: 1},
                                                        1200: {items: 1},
                                                        1400: {items: 1}
                                                    },
                                                    dotClass: "owl2-dot",
                                                    dotsClass: "owl2-dots",
                                                    dots: false,
                                                    dotsSpeed: 500,
                                                    nav: false,
                                                    loop: false,
                                                    navSpeed: 500,
                                                    navText: ["&#171 ", "&#187 "],
                                                    navClass: ["owl2-prev", "owl2-next"]

                                                });

                                                $extraslider.on("translate.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    _UngetAnimate($item_active);
                                                    _getAnimate($item_active);
                                                });

                                                $extraslider.on("translated.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    var $item = $(".owl2-item", $element);

                                                    _UngetAnimate($item);

                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {

                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });

                                                    }
                                                });

                                                function _getAnimate($el) {
                                                    if (_effect == "none") return;
                                                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                                    $extraslider.removeClass("extra-animate");
                                                    $el.each(function (i) {
                                                        var $_el = $(this);
                                                        $(this).css({
                                                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                            "-o-animation": _effect + " " + _duration + "ms ease both",
                                                            "animation": _effect + " " + _duration + "ms ease both",
                                                            "-webkit-animation-delay": +i * _delay + "ms",
                                                            "-moz-animation-delay": +i * _delay + "ms",
                                                            "-o-animation-delay": +i * _delay + "ms",
                                                            "animation-delay": +i * _delay + "ms",
                                                            "opacity": 1
                                                        }).animate({
                                                            opacity: 1
                                                        });

                                                        if (i == $el.size() - 1) {
                                                            $extraslider.addClass("extra-animate");
                                                        }
                                                    });
                                                }

                                                function _UngetAnimate($el) {
                                                    $el.each(function (i) {
                                                        $(this).css({
                                                            "animation": "",
                                                            "-webkit-animation": "",
                                                            "-moz-animation": "",
                                                            "-o-animation": "",
                                                            "opacity": 1
                                                        });
                                                    });
                                                }

                                            })("#so_extra_slider_3849357241573891478 ");
                                        });
                                        //]]>
                                    </script>

                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_l3lb block hidden-xs">
                        <div class="banner-layout-3 bt-1 clearfix banners">
                            <div class="">
                                <a class="bn-shadow" href="#" title="Banner 1">
                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/01.jpg"
                                         alt="Static Image">
                                </a>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_z8xz block">

                        <div class="module so-extraslider-ltr home3_extra_style2 bn-shadow">

                            <h3 class="modtitle">Recommend Items</h3>


                            <div class="modcontent">


                                <div id="so_extra_slider_15914538661573891478"
                                     class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-2 preset04-1 button-type1">
                                    <div class="box-banner">
                                        <div class="banners">
                                        </div>
                                    </div>
                                    <!-- Begin extraslider-inner -->
                                    <div class="extraslider-inner products-list grid" data-effect="none">

                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='104'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                               target="_self"
                                                               title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/25-270x270.jpg"
                                                                     alt="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-82% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                                   target="_self"
                                                                   title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2) ">
                                                                    Toshiba Pro 21&quot;(21:9) FHD IPS LED
                                                                    1920X1080 HDMI(2)
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$337.99 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('104');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('104');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('104');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='66'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                               target="_self"
                                                               title="Compact Portable Charger (Power Bank) with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                                     alt="Compact Portable Charger (Power Bank) with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-70% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                   target="_self"
                                                                   title="Compact Portable Charger (Power Bank) with Premium ">
                                                                    Compact Portable Charger (Power Bank)
                                                                    with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$241.99 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('66');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('66');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('66');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='105'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                               target="_self"
                                                               title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                     alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-40% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium ">
                                                                    Lorem Ipsum dolor at vero eos et iusto
                                                                    odi with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('105');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('105');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('105');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='30'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                               target="_self"
                                                               title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                                     alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-20% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus ">
                                                                    Magnetic Air Vent Phone Holder for
                                                                    iPhone 7 / 7 Plus
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('30');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('30');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('30');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='51'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                               target="_self"
                                                               title="Charger  Compact Portable with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                     alt="Charger  Compact Portable with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-20% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium ">
                                                                    Charger Compact Portable with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('51');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('51');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('51');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='103'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                               target="_self"
                                                               title="Compact Portable Charger (External Battery)">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                                     alt="Compact Portable Charger (External Battery)">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                   target="_self"
                                                                   title="Compact Portable Charger (External Battery) ">
                                                                    Compact Portable Charger (External
                                                                    Battery)
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

														<span class="price product-price">
															$98.00
														</span>


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('103');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('103');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('103');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                    </div>
                                    <!--End extraslider-inner -->

                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            (function (element) {
                                                var $element = $(element),
                                                    $extraslider = $(".extraslider-inner", $element),
                                                    _delay = 500,
                                                    _duration = 800,
                                                    _effect = 'none ';

                                                $extraslider.on("initialized.owl.carousel2", function () {
                                                    var $item_active = $(".owl2-item.active", $element);
                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {
                                                        var $item = $(".owl2-item", $element);
                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });
                                                    }


                                                    $(".owl2-controls", $element).insertBefore($extraslider);
                                                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                                });

                                                $extraslider.owlCarousel2({
                                                    rtl: false,
                                                    margin: 0,
                                                    slideBy: 1,
                                                    autoplay: 0,
                                                    autoplayHoverPause: 0,
                                                    autoplayTimeout: 0,
                                                    autoplaySpeed: 1000,
                                                    startPosition: 0,
                                                    mouseDrag: 1,
                                                    touchDrag: 1,
                                                    autoWidth: false,
                                                    responsive: {
                                                        0: {items: 1},
                                                        480: {items: 2},
                                                        768: {items: 1},
                                                        1200: {items: 1},
                                                        1400: {items: 1}
                                                    },
                                                    dotClass: "owl2-dot",
                                                    dotsClass: "owl2-dots",
                                                    dots: false,
                                                    dotsSpeed: 500,
                                                    nav: false,
                                                    loop: false,
                                                    navSpeed: 500,
                                                    navText: ["&#171 ", "&#187 "],
                                                    navClass: ["owl2-prev", "owl2-next"]

                                                });

                                                $extraslider.on("translate.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    _UngetAnimate($item_active);
                                                    _getAnimate($item_active);
                                                });

                                                $extraslider.on("translated.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    var $item = $(".owl2-item", $element);

                                                    _UngetAnimate($item);

                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {

                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });

                                                    }
                                                });

                                                function _getAnimate($el) {
                                                    if (_effect == "none") return;
                                                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                                    $extraslider.removeClass("extra-animate");
                                                    $el.each(function (i) {
                                                        var $_el = $(this);
                                                        $(this).css({
                                                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                            "-o-animation": _effect + " " + _duration + "ms ease both",
                                                            "animation": _effect + " " + _duration + "ms ease both",
                                                            "-webkit-animation-delay": +i * _delay + "ms",
                                                            "-moz-animation-delay": +i * _delay + "ms",
                                                            "-o-animation-delay": +i * _delay + "ms",
                                                            "animation-delay": +i * _delay + "ms",
                                                            "opacity": 1
                                                        }).animate({
                                                            opacity: 1
                                                        });

                                                        if (i == $el.size() - 1) {
                                                            $extraslider.addClass("extra-animate");
                                                        }
                                                    });
                                                }

                                                function _UngetAnimate($el) {
                                                    $el.each(function (i) {
                                                        $(this).css({
                                                            "animation": "",
                                                            "-webkit-animation": "",
                                                            "-moz-animation": "",
                                                            "-o-animation": "",
                                                            "opacity": 1
                                                        });
                                                    });
                                                }

                                            })("#so_extra_slider_15914538661573891478 ");
                                        });
                                        //]]>
                                    </script>

                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_ydxt block">
                        <div class="block-testimonial bn-shadow">
                            <div class="testimonial-items contentslider" data-rtl="no" data-loop="no"
                                 data-autoplay="yes" data-autoheight="no" data-autowidth="no" data-delay="4"
                                 data-speed="0.6" data-margin="0" data-items_column0="1"
                                 data-items_column1="1" data-items_column2="1" data-items_column3="1"
                                 data-items_column4="1" data-arrows="no" data-pagination="yes"
                                 data-lazyload="yes" data-hoverpause="yes">
                                <div class="item">
                                    <div class="text">
                                        <div class="t">Lorem Khaled Ipsum is a major key to success. It’s on
                                            you how you want to live your life. Everyone has a choice. I
                                            pick my choice, squeaky clean. Always remember in the jungle
                                            there’s a lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-2.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">Sharon Stone</div>
                                    <div class="job">Acc - Hollywood</div>
                                </div>
                                <div class="item">
                                    <div class="text">
                                        <div class="t">
                                            Khaled Lorem Ipsum is a major key to success. It’s on you how
                                            you want to live your life. Everyone has a choice. I pick my
                                            choice, squeaky clean. Always remember in the jungle there’s a
                                            lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-1.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">David Beckham</div>
                                    <div class="job">CE0 - Magentech</div>
                                </div>
                                <div class="item">
                                    <div class="text">
                                        <div class="t">
                                            Lorem Khaled Ipsum is a major key to success. It’s on you how
                                            you want to live your life. Everyone has a choice. I pick my
                                            choice, squeaky clean. Always remember in the jungle there’s a
                                            lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-3.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">Johny Walker</div>
                                    <div class="job">Manager - United</div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 col_2w6x  slider_container">

                <div class="row row_w6so  row-style ">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_6iia block">
                        <script>
                            //<![CDATA[
                            var listdeal1 = [];
                            //]]>
                        </script>
                        <div class="module so-deals-ltr home3_deal_style2">
                            <div class="head-title">
                                <h2 class="modtitle font-ct"><span>Hot Deal</span></h2>

                                <div class="cslider-item-timer">
                                    <div class="product_time_maxprice"></div>
                                </div>

                                <script type="text/javascript">
                                    //<![CDATA[
                                    listdeal1.push('product_time_maxprice|2020/10/31 00:00:00')
                                    //]]>
                                </script>
                            </div>
                            <div class="modcontent products-list grid">
                                <div id="so_deals_12662999911162019080438"
                                     class="so-deal modcontent products-list grid clearfix preset00-4 preset01-4 preset02-3 preset03-2 preset04-1  button-type1  style2">
                                    <div class="extraslider-inner" data-effect="none">
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='30'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                                         alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                                    Magnetic Air Vent Phone Holder for
                                                                    iPhone 7 / 7 Plus</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('30');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('30');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('30');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='51'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                         alt="Charger  Compact Portable with Premium"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium">Charger
                                                                    Compact Portable with Premium</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('51');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('51');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('51');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='58'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self"
                                                                   title="Computer Science saepe eveniet ut et volu redae">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                                         alt="Computer Science saepe eveniet ut et volu redae"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-2%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self"
                                                                   title="Computer Science saepe eveniet ut et volu redae">Computer
                                                                    Science saepe eveniet ut et volu
                                                                    redae</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$119.60</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('58');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('58');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('58');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='105'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                         alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-40%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">Lorem
                                                                    Ipsum dolor at vero eos et iusto odi
                                                                    with Premium</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$74.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('105');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('105');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('105');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='78'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self"
                                                                   title="Portable  Compact Charger (External Battery) t45">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                                         alt="Portable  Compact Charger (External Battery) t45"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self"
                                                                   title="Portable  Compact Charger (External Battery) t45">Portable
                                                                    Compact Charger (External Battery)
                                                                    t45</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('78');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('78');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('78');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='86'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self"
                                                                   title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/29-270x270.jpg"
                                                                         alt="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-11%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self"
                                                                   title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">Seneo
                                                                    PA046 Fast Charger Wireless
                                                                    Sleep-Friendly)</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$108.80</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('86');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('86');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('86');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    jQuery(document).ready(function ($) {
                                        ;
                                        (function (element) {
                                            var $element = $(element),
                                                $extraslider = $('.extraslider-inner', $element),
                                                $featureslider = $('.product-feature', $element),
                                                _delay = 500,
                                                _duration = 800,
                                                _effect = 'none';

                                            $extraslider.on('initialized.owl.carousel2', function () {
                                                var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    var $item = $('.extraslider-inner .owl2-item', $element);
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                                $('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));
                                                $('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');
                                            });

                                            $extraslider.owlCarousel2({
                                                rtl: false,
                                                margin: 1,
                                                slideBy: 1,
                                                autoplay: false,
                                                autoplayHoverPause: 0,
                                                autoplayTimeout: 5000,
                                                autoplaySpeed: 1000,
                                                startPosition: 0,
                                                mouseDrag: true,
                                                touchDrag: true,
                                                autoWidth: false,
                                                responsive: {
                                                    0: {items: 1},
                                                    480: {items: 2},
                                                    768: {items: 3},
                                                    992: {items: 4},
                                                    1200: {items: 4}
                                                },
                                                dotClass: 'owl2-dot',
                                                dotsClass: 'owl2-dots',
                                                dots: false,
                                                dotsSpeed: 500,
                                                nav: false,
                                                loop: true,
                                                navSpeed: 500,
                                                navText: ['&#171;', '&#187;'],
                                                navClass: ['owl2-prev', 'owl2-next']
                                            });

                                            $extraslider.on('translated.owl.carousel2', function (e) {
                                                var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                                                var $item = $('.extraslider-inner .owl2-item', $element);

                                                _UngetAnimate($item);

                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                            });
                                            /*feature product*/
                                            $featureslider.on('initialized.owl.carousel2', function () {
                                                var $item_active = $('.product-feature .owl2-item.active', $element);
                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    var $item = $('.owl2-item', $element);
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                                $('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));
                                                $('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');
                                            });

                                            $featureslider.owlCarousel2({
                                                rtl: false,
                                                margin: 1,
                                                slideBy: 1,
                                                autoplay: false,
                                                autoplayHoverPause: 0,
                                                autoplayTimeout: 5000,
                                                autoplaySpeed: 1000,
                                                startPosition: 0,
                                                mouseDrag: true,
                                                touchDrag: true,
                                                autoWidth: false,
                                                responsive: {
                                                    0: {items: 1},
                                                    480: {items: 1},
                                                    768: {items: 1},
                                                    992: {items: 1},
                                                    1200: {items: 1}
                                                },
                                                dotClass: 'owl2-dot',
                                                dotsClass: 'owl2-dots',
                                                dots: false,
                                                dotsSpeed: 500,
                                                nav: false,
                                                loop: true,
                                                navSpeed: 500,
                                                navText: ['&#171;', '&#187;'],
                                                navClass: ['owl2-prev', 'owl2-next']
                                            });

                                            $featureslider.on('translated.owl.carousel2', function (e) {
                                                var $item_active = $('.product-feature .owl2-item.active', $element);
                                                var $item = $('.product-feature .owl2-item', $element);

                                                _UngetAnimate($item);

                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                            });

                                            function _getAnimate($el) {
                                                if (_effect == 'none') return;
                                                $extraslider.removeClass('extra-animate');
                                                $el.each(function (i) {
                                                    var $_el = $(this);
                                                    $(this).css({
                                                        '-webkit-animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-moz-animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-o-animation': _effect + ' ' + _duration + "ms ease both",
                                                        'animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-webkit-animation-delay': +i * _delay + 'ms',
                                                        '-moz-animation-delay': +i * _delay + 'ms',
                                                        '-o-animation-delay': +i * _delay + 'ms',
                                                        'animation-delay': +i * _delay + 'ms',
                                                        'opacity': 1
                                                    }).animate({
                                                        opacity: 1
                                                    });

                                                    if (i == $el.size() - 1) {
                                                        $extraslider.addClass("extra-animate");
                                                    }
                                                });
                                            }

                                            function _UngetAnimate($el) {
                                                $el.each(function (i) {
                                                    $(this).css({
                                                        'animation': '',
                                                        '-webkit-animation': '',
                                                        '-moz-animation': '',
                                                        '-o-animation': '',
                                                        'opacity': 1
                                                    });
                                                });
                                            }

                                            data = new Date(2013, 10, 26, 12, 00, 00);

                                            function CountDown(date, id) {
                                                dateNow = new Date();
                                                amount = date.getTime() - dateNow.getTime();
                                                if (amount < 0 && $('#' + id).length) {
                                                    $('.' + id).html("Now!");
                                                } else {
                                                    days = 0;
                                                    hours = 0;
                                                    mins = 0;
                                                    secs = 0;
                                                    out = "";
                                                    amount = Math.floor(amount / 1000);
                                                    days = Math.floor(amount / 86400);
                                                    amount = amount % 86400;
                                                    hours = Math.floor(amount / 3600);
                                                    amount = amount % 3600;
                                                    mins = Math.floor(amount / 60);
                                                    amount = amount % 60;
                                                    secs = Math.floor(amount);
                                                    if (days != 0) {
                                                        out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "D :" : "D :") + "</div>" + "</div> ";
                                                    }
                                                    if (days == 0 && hours != 0) {
                                                        out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "H :" : "H :") + "</div>" + "</div> ";
                                                    } else if (hours != 0) {
                                                        out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "H :" : "H :") + "</div>" + "</div> ";
                                                    }
                                                    if (days == 0 && hours != 0) {
                                                        out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    } else if (days == 0 && hours == 0) {
                                                        out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    } else {
                                                        out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    }

                                                    $('.' + id).html(out);

                                                    setTimeout(function () {
                                                        CountDown(date, id);
                                                    }, 1000);
                                                }
                                            }

                                            if (listdeal1.length > 0) {
                                                for (var i = 0; i < listdeal1.length; i++) {
                                                    var arr = listdeal1[i].split("|");
                                                    if (arr[1].length) {
                                                        var data = new Date(arr[1]);
                                                        CountDown(data, arr[0]);
                                                    }
                                                }
                                            }
                                        })('#so_deals_12662999911162019080438');
                                    });
                                    //]]>
                                </script>
                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_kjmz block">
                        <div class="banner-21 banner">
                            <div>
                                <a class="bn-shadow" href="#" title="Banner 24">
                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner21.jpg"
                                         alt="Static Image">
                                </a>
                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_b5i1 block">

                        <div class="module so-listing-tabs-ltr home3_listingtab">
                            <div class="head-title">
                                <h3 class="modtitle">Fashion & Accessories</h3>
                            </div>
                            <div class="modcontent">
                                <!--[if lt IE 9]>
                                <div id="so_listing_tabs_244" class="so-listing-tabs msie lt-ie9 first-load module"><![endif]-->
                                <!--[if IE 9]>
                                <div id="so_listing_tabs_244" class="so-listing-tabs msie first-load module"><![endif]-->
                                <!--[if gt IE 9]><!-->
                                <div id="so_listing_tabs_244" class="so-listing-tabs first-load module">
                                    <!--<![endif]-->
                                    <div class="ltabs-wrap products-list grid">
                                        <div class="ltabs-tabs-container" data-delay="500"
                                             data-duration="800"
                                             data-effect="none"
                                             data-ajaxurl="http://opencart.opencartworks.com/themes/so_topdeal3/"
                                             data-type_source="0"
                                             data-type_show="slider">

                                            <div class="ltabs-tabs-wrap">
                                                <span class='ltabs-tab-selected'></span>
                                                <span class="ltabs-tab-arrow">▼</span>
                                                <ul class="ltabs-tabs cf">
                                                    <li class="ltabs-tab   tab-sel tab-loaded "
                                                        data-category-id="32"
                                                        data-active-content-l=".items-category-32"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/116-60x60.jpg"
                                                                 title="Bags" alt="Bags"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Bags
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="36"
                                                        data-active-content-l=".items-category-36"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/23-60x60.jpg"
                                                                 title="Dress Ladies" alt="Dress Ladies"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Dress Ladies
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="31"
                                                        data-active-content-l=".items-category-31"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/24-60x60.jpg"
                                                                 title="Jean" alt="Jean"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Jean
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="27"
                                                        data-active-content-l=".items-category-27"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/9-60x60.jpg"
                                                                 title="Men Fashion" alt="Men Fashion"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Men Fashion
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="88"
                                                        data-active-content-l=".items-category-88"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/22-60x60.jpg"
                                                                 title="T-shirt" alt="T-shirt"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													T-shirt
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="26"
                                                        data-active-content-l=".items-category-26"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/25-60x60.jpg"
                                                                 title="Trending" alt="Trending"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Trending
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="37"
                                                        data-active-content-l=".items-category-37"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/21-60x60.jpg"
                                                                 title="Western Wear" alt="Western Wear"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Western Wear
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="20"
                                                        data-active-content-l=".items-category-20"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/10-60x60.jpg"
                                                                 title="Women Fashion" alt="Women Fashion"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Women Fashion
											</span>
                                                    </li>
                                                </ul>
                                            </div>


                                        </div>
                                        <div class="wap-listing-tabs">
                                            <div class="ltabs-items-container">
                                                <div class="ltabs-items  ltabs-items-selected ltabs-items-loaded items-category-32"
                                                     data-total="6">

                                                    <div class="ltabs-items-inner owl2-carousel  ltabs-slider ">

                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='42'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                               target="_self"
                                                                               title="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/24-270x270.png"
                                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz"
                                                                                   target="_self">
                                                                                    Est Officia Including
                                                                                    Shoes Beautiful Pieces
                                                                                    Canaz
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('42');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('42');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('42');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='52'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=52"
                                                                               target="_self"
                                                                               title="Invisible Hidden Spy Earphone Micro Wireless">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/16-270x270.png"
                                                                                     alt="Invisible Hidden Spy Earphone Micro Wireless">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=52"
                                                                                   title="Invisible Hidden Spy Earphone Micro Wireless"
                                                                                   target="_self">
                                                                                    Invisible Hidden Spy
                                                                                    Earphone Micro Wireless
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('52');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('52');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('52');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='87'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=87"
                                                                               target="_self"
                                                                               title="Ligula tortoram ut labore et dolore magna elip">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/14-270x270.png"
                                                                                     alt="Ligula tortoram ut labore et dolore magna elip">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=87"
                                                                                   title="Ligula tortoram ut labore et dolore magna elip"
                                                                                   target="_self">
                                                                                    Ligula tortoram ut
                                                                                    labore et dolore magna
                                                                                    elip
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('87');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('87');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('87');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='90'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=90"
                                                                               target="_self"
                                                                               title="Ligula tortoram ut labore et dolore magna elip">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/11-270x270.png"
                                                                                     alt="Ligula tortoram ut labore et dolore magna elip">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=90"
                                                                                   title="Ligula tortoram ut labore et dolore magna elip"
                                                                                   target="_self">
                                                                                    Ligula tortoram ut
                                                                                    labore et dolore magna
                                                                                    elip
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('90');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('90');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('90');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='83'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=83"
                                                                               target="_self"
                                                                               title="magna elip therefore always free from bolac sodo">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/18-270x270.png"
                                                                                     alt="magna elip therefore always free from bolac sodo">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-2%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=83"
                                                                                   title="magna elip therefore always free from bolac sodo"
                                                                                   target="_self">
                                                                                    magna elip therefore
                                                                                    always free from bolac
                                                                                    sodo
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$119.60</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('83');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('83');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('83');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='29'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                               target="_self"
                                                                               title="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/15-270x270.png"
                                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-82%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz"
                                                                                   target="_self">
                                                                                    Est Officia Including
                                                                                    Shoes Beautiful Pieces
                                                                                    Canaz
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$62.00</span>
                                                                                <span class="price-old">$337.99</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('29');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('29');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('29');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            var $tag_id = $('#so_listing_tabs_244'),
                                                                parent_active = $('.items-category-32', $tag_id),
                                                                total_product = parent_active.data('total'),
                                                                tab_active = $('.ltabs-items-inner', parent_active),
                                                                nb_column0 = 2,
                                                                nb_column1 = 2,
                                                                nb_column2 = 1,
                                                                nb_column3 = 2,
                                                                nb_column4 = 1;
                                                            tab_active.owlCarousel2({
                                                                rtl: false,
                                                                nav: false,
                                                                dots: false,
                                                                margin: 0,
                                                                loop: false,
                                                                autoplay: false,
                                                                autoplayHoverPause: false,
                                                                autoplayTimeout: 5000,
                                                                autoplaySpeed: 1000,
                                                                mouseDrag: true,
                                                                touchDrag: true,
                                                                navRewind: true,
                                                                navText: ['', ''],
                                                                responsive: {
                                                                    0: {
                                                                        items: nb_column4,
                                                                        nav: total_product <= nb_column4 ? false : true,
                                                                    },
                                                                    480: {
                                                                        items: nb_column3,
                                                                        nav: total_product <= nb_column3 ? false : true,
                                                                    },
                                                                    768: {
                                                                        items: nb_column2,
                                                                        nav: total_product <= nb_column2 ? false : true,
                                                                    },
                                                                    992: {
                                                                        items: nb_column1,
                                                                        nav: total_product <= nb_column1 ? false : true,
                                                                    },
                                                                    1200: {
                                                                        items: nb_column0,
                                                                        nav: total_product <= nb_column0 ? false : true,
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    </script>


                                                </div>
                                                <div class="ltabs-items  items-category-36" data-total="6">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-31" data-total="3">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-27" data-total="10">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-88" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-26" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-37" data-total="4">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-20" data-total="9">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            ;
                                            (function (element) {
                                                var $element = $(element),
                                                    $tab = $('.ltabs-tab', $element),
                                                    $tab_label = $('.ltabs-tab-label', $tab),
                                                    $tabs = $('.ltabs-tabs', $element),
                                                    ajax_url = $tabs.parents('.ltabs-tabs-container').attr('data-ajaxurl'),
                                                    effect = $tabs.parents('.ltabs-tabs-container').attr('data-effect'),
                                                    delay = $tabs.parents('.ltabs-tabs-container').attr('data-delay'),
                                                    duration = $tabs.parents('.ltabs-tabs-container').attr('data-duration'),
                                                    type_source = $tabs.parents('.ltabs-tabs-container').attr('data-type_source'),
                                                    $items_content = $('.ltabs-items', $element),
                                                    $items_inner = $('.ltabs-items-inner', $items_content),
                                                    $items_first_active = $('.ltabs-items-selected', $element),
                                                    $load_more = $('.ltabs-loadmore', $element),
                                                    $btn_loadmore = $('.ltabs-loadmore-btn', $load_more),
                                                    $select_box = $('.ltabs-selectbox', $element),
                                                    $tab_label_select = $('.ltabs-tab-selected', $element),
                                                    setting = 'a:73:{s:6:"action";s:9:"save_edit";s:4:"name";s:34:"Home 3 - Fashion &amp; Accessories";s:18:"module_description";a:2:{i:2;a:1:{s:9:"head_name";s:25:"Fashion &amp; Accessories";}i:1;a:1:{s:9:"head_name";s:25:"Fashion &amp; Accessories";}}s:9:"head_name";s:25:"Fashion &amp; Accessories";s:17:"disp_title_module";s:1:"1";s:6:"status";s:1:"1";s:12:"store_layout";s:6:"style3";s:12:"class_suffix";s:16:"home3_listingtab";s:16:"item_link_target";s:5:"_self";s:10:"nb_column0";s:1:"2";s:10:"nb_column1";s:1:"2";s:10:"nb_column2";s:1:"1";s:10:"nb_column3";s:1:"2";s:10:"nb_column4";s:1:"1";s:9:"type_show";s:6:"slider";s:6:"nb_row";s:1:"2";s:11:"type_source";s:1:"0";s:8:"category";a:8:{i:0;s:2:"32";i:1;s:2:"20";i:2;s:2:"88";i:3;s:2:"26";i:4;s:2:"27";i:5;s:2:"31";i:6;s:2:"36";i:7;s:2:"37";}s:14:"child_category";s:1:"1";s:14:"category_depth";s:1:"1";s:12:"product_sort";s:7:"p.price";s:16:"product_ordering";s:3:"ASC";s:12:"source_limit";s:1:"6";s:13:"catid_preload";s:2:"32";s:17:"field_product_tab";s:0:"";s:15:"tab_all_display";s:1:"0";s:18:"tab_max_characters";s:2:"25";s:16:"tab_icon_display";s:1:"1";s:12:"cat_order_by";s:4:"name";s:15:"imgcfgcat_width";s:2:"60";s:16:"imgcfgcat_height";s:2:"60";s:13:"display_title";s:1:"1";s:15:"title_maxlength";s:2:"50";s:19:"display_description";s:1:"0";s:21:"description_maxlength";s:3:"100";s:13:"display_price";s:1:"1";s:19:"display_add_to_cart";s:1:"1";s:16:"display_wishlist";s:1:"1";s:15:"display_compare";s:1:"1";s:14:"display_rating";s:1:"1";s:12:"display_sale";s:1:"1";s:11:"display_new";s:1:"1";s:8:"date_day";s:3:"227";s:17:"product_image_num";s:1:"1";s:13:"product_image";s:1:"1";s:22:"product_get_image_data";s:1:"1";s:23:"product_get_image_image";s:1:"1";s:5:"width";s:3:"270";s:6:"height";s:3:"270";s:24:"product_placeholder_path";s:11:"http://opencart.opencartworks.com/themes/so_topdeal3/nophoto.png";s:20:"display_banner_image";s:1:"0";s:12:"banner_image";s:12:"http://opencart.opencartworks.com/themes/so_topdeal3/no_image.png";s:12:"banner_width";s:3:"150";s:13:"banner_height";s:3:"250";s:16:"banner_image_url";s:0:"";s:6:"effect";s:4:"none";s:8:"duration";s:3:"800";s:5:"delay";s:3:"500";s:8:"autoplay";s:1:"0";s:11:"display_nav";s:1:"1";s:12:"display_dots";s:1:"0";s:12:"display_loop";s:1:"0";s:9:"touchdrag";s:1:"1";s:9:"mousedrag";s:1:"1";s:10:"pausehover";s:1:"0";s:15:"autoplayTimeout";s:4:"5000";s:13:"autoplaySpeed";s:4:"1000";s:8:"pre_text";s:0:"";s:9:"post_text";s:0:"";s:9:"use_cache";s:1:"0";s:10:"cache_time";s:4:"3600";s:8:"moduleid";s:3:"244";s:5:"start";i:0;}',
                                                    type_show = 'slider';

                                                enableSelectBoxes();

                                                function enableSelectBoxes() {
                                                    $tab_wrap = $('.ltabs-tabs-wrap', $element),
                                                        $tab_label_select.html($('.ltabs-tab', $element).filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    if ($(window).innerWidth() <= 479) {
                                                        $tab_wrap.addClass('ltabs-selectbox');
                                                    } else {
                                                        $tab_wrap.removeClass('ltabs-selectbox');
                                                    }
                                                }

                                                $('span.ltabs-tab-selected, span.ltabs-tab-arrow', $element).click(function () {
                                                    if ($('.ltabs-tabs', $element).hasClass('ltabs-open')) {
                                                        $('.ltabs-tabs', $element).removeClass('ltabs-open');
                                                    } else {
                                                        $('.ltabs-tabs', $element).addClass('ltabs-open');
                                                    }
                                                });

                                                $(window).resize(function () {
                                                    if ($(window).innerWidth() <= 479) {
                                                        $('.ltabs-tabs-wrap', $element).addClass('ltabs-selectbox');
                                                    } else {
                                                        $('.ltabs-tabs-wrap', $element).removeClass('ltabs-selectbox');
                                                    }
                                                });

                                                function showAnimateItems(el) {
                                                    var $_items = $('.new-ltabs-item', el), nub = 0;
                                                    $('.ltabs-loadmore-btn', el).fadeOut('fast');
                                                    $_items.each(function (i) {
                                                        nub++;
                                                        switch (effect) {
                                                            case 'none' :
                                                                $(this).css({
                                                                    'opacity': '1',
                                                                    'filter': 'alpha(opacity = 100)'
                                                                });
                                                                break;
                                                            default:
                                                                animatesItems($(this), nub * delay, i, el);
                                                        }
                                                        if (i == $_items.length - 1) {
                                                            $('.ltabs-loadmore-btn', el).fadeIn(3000);
                                                        }
                                                        $(this).removeClass('new-ltabs-item');
                                                    });
                                                }

                                                function animatesItems($this, fdelay, i, el) {
                                                    var $_items = $('.ltabs-item', el);
                                                    $this.stop(true, true).attr("style",
                                                        "-webkit-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation:" + effect + " " + duration + "ms;"
                                                        + "-o-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation-delay:" + fdelay + "ms;"
                                                        + "-webkit-animation-delay:" + fdelay + "ms;"
                                                        + "-o-animation-delay:" + fdelay + "ms;"
                                                        + "animation-delay:" + fdelay + "ms;").delay(fdelay).animate({
                                                        opacity: 1,
                                                        filter: 'alpha(opacity = 100)'
                                                    }, {
                                                        delay: 1000
                                                    });
                                                    if (i == ($_items.length - 1)) {
                                                        $(".ltabs-items-inner").addClass("play");
                                                    }
                                                }

                                                if (type_show == 'loadmore') {
                                                    showAnimateItems($items_first_active);
                                                }
                                                $tab.on('click.ltabs-tab', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('tab-sel')) return false;
                                                    if ($this.parents('.ltabs-tabs').hasClass('ltabs-open')) {
                                                        $this.parents('.ltabs-tabs').removeClass('ltabs-open');
                                                    }
                                                    $tab.removeClass('tab-sel');
                                                    $this.addClass('tab-sel');
                                                    var items_active = $this.attr('data-active-content-l');
                                                    var _items_active = $(items_active, $element);
                                                    $items_content.removeClass('ltabs-items-selected');
                                                    _items_active.addClass('ltabs-items-selected');
                                                    $tab_label_select.html($tab.filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    var $loading = $('.ltabs-loading', _items_active);
                                                    var loaded = _items_active.hasClass('ltabs-items-loaded');
                                                    type_show = $tabs.parents('.ltabs-tabs-container').attr('data-type_show');
                                                    if (!loaded && !_items_active.hasClass('ltabs-process')) {
                                                        _items_active.addClass('ltabs-process');
                                                        var category_id = $this.attr('data-category-id');
                                                        $loading.show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: ajax_url,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: 0,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 244
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $('.ltabs-loading', _items_active).replaceWith(data.items_markup);
                                                                    _items_active.addClass('ltabs-items-loaded').removeClass('ltabs-process');
                                                                    $loading.remove();
                                                                    if (type_show != 'slider') {
                                                                        showAnimateItems(_items_active);
                                                                    }
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }

                                                            },
                                                            dataType: 'json'
                                                        });

                                                    } else {
                                                        if (type_show == 'loadmore') {
                                                            $('.ltabs-item', $items_content).removeAttr('style').addClass('new-ltabs-item');
                                                            showAnimateItems(_items_active);
                                                        } else {
                                                            var owl = $('.owl2-carousel', _items_active);
                                                            owl = owl.data('owlCarousel2');
                                                            if (typeof owl !== 'undefined') {
                                                                owl.onResize();
                                                            }
                                                        }
                                                    }
                                                });

                                                function updateStatus($el) {
                                                    $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    var countitem = $('.ltabs-item', $el).length;
                                                    $('.ltabs-image-loading', $el).css({display: 'none'});
                                                    $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_start', countitem);
                                                    var rl_total = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_total');
                                                    var rl_load = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_load');
                                                    var rl_allready = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_allready');

                                                    if (countitem >= rl_total) {
                                                        $('.ltabs-loadmore-btn', $el).addClass('loaded');
                                                        $('.ltabs-image-loading', $el).css({display: 'none'});
                                                        $('.ltabs-loadmore-btn', $el).attr('data-label', rl_allready);
                                                        $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    }
                                                }

                                                $btn_loadmore.on('click.loadmore', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('loaded') || $this.hasClass('loading')) {
                                                        return false;
                                                    } else {
                                                        $this.addClass('loading');
                                                        $('.ltabs-image-loading', $this).css({display: 'inline-block'});
                                                        var rl_start = $this.parent().attr('data-rl_start'),
                                                            rl_ajaxurl = $this.parent().attr('data-ajaxurl'),
                                                            effect = $this.parent().attr('data-effect'),
                                                            category_id = $this.parent().attr('data-categoryid'),
                                                            items_active = $this.parent().attr('data-active-content');

                                                        var _items_active = $(items_active, $element);

                                                        $.ajax({
                                                            type: 'POST',
                                                            url: rl_ajaxurl,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: rl_start,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 244
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $($(data.items_markup).html()).insertAfter($('.ltabs-item', _items_active).nextAll().last());
                                                                    $('.ltabs-image-loading', $this).css({display: 'none'});
                                                                    showAnimateItems(_items_active);
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }
                                                            }, dataType: 'json'
                                                        });
                                                    }
                                                    return false;
                                                });
                                            })('#so_listing_tabs_244');
                                        });
                                        //]]>
                                    </script>
                                </div>
                            </div> <!-- /.modcontent-->

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_gmfs block">

                        <div class="module so-listing-tabs-ltr home3_listingtab_style2">
                            <div class="head-title">
                                <h3 class="modtitle"><span>Digital & Electronic</span></h3>
                            </div>
                            <div class="modcontent">
                                <!--[if lt IE 9]>
                                <div id="so_listing_tabs_246" class="so-listing-tabs msie lt-ie9 first-load module"><![endif]-->
                                <!--[if IE 9]>
                                <div id="so_listing_tabs_246" class="so-listing-tabs msie first-load module"><![endif]-->
                                <!--[if gt IE 9]><!-->
                                <div id="so_listing_tabs_246" class="so-listing-tabs first-load module">
                                    <!--<![endif]-->
                                    <div class="ltabs-wrap">
                                        <div class="ltabs-tabs-container" data-delay="500"
                                             data-duration="800"
                                             data-effect="none"
                                             data-ajaxurl="http://opencart.opencartworks.com/themes/so_topdeal3/"
                                             data-type_source="0"
                                             data-type_show="slider">

                                            <div class="ltabs-tabs-wrap">
                                                <span class='ltabs-tab-selected'></span>
                                                <span class="ltabs-tab-arrow">▼</span>
                                                <ul class="ltabs-tabs cf">
                                                    <li class="ltabs-tab   tab-sel tab-loaded "
                                                        data-category-id="28"
                                                        data-active-content-l=".items-category-28"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/94-60x60.jpg"
                                                                 title="Case" alt="Case"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Case
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="30"
                                                        data-active-content-l=".items-category-30"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/93-60x60.jpg"
                                                                 title="Cell &amp; Cable"
                                                                 alt="Cell &amp; Cable"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Cell &amp; Cable
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="89"
                                                        data-active-content-l=".items-category-89"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/88-60x60.jpg"
                                                                 title="Headphone" alt="Headphone"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Headphone
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="90"
                                                        data-active-content-l=".items-category-90"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/90-60x60.jpg"
                                                                 title="Laptops" alt="Laptops"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Laptops
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="77"
                                                        data-active-content-l=".items-category-77"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/87-60x60.jpg"
                                                                 title="Mobile &amp; Table"
                                                                 alt="Mobile &amp; Table"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Mobile &amp; Table
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="79"
                                                        data-active-content-l=".items-category-79"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/91-60x60.jpg"
                                                                 title="Sound" alt="Sound"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Sound
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="80"
                                                        data-active-content-l=".items-category-80"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/89-60x60.jpg"
                                                                 title="USB &amp; HDD" alt="USB &amp; HDD"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													USB &amp; HDD
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="62"
                                                        data-active-content-l=".items-category-62"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/92-60x60.jpg"
                                                                 title="Video &amp; Camera"
                                                                 alt="Video &amp; Camera"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Video &amp; Camera
											</span>
                                                    </li>
                                                </ul>
                                            </div>


                                        </div>
                                        <div class="wap-listing-tabs products-list grid">

                                            <div class="ltabs-items-container">
                                                <div class="ltabs-items  ltabs-items-selected ltabs-items-loaded items-category-28"
                                                     data-total="15">

                                                    <div class="ltabs-items-inner owl2-carousel  ltabs-slider ">

                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='103'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery)">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery)">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                                   title="Compact Portable Charger (External Battery)"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $98.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('103');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('103');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('103');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='51'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                               target="_self"
                                                                               title="Charger  Compact Portable with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                                     alt="Charger  Compact Portable with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                                   title="Charger  Compact Portable with Premium"
                                                                                   target="_self">
                                                                                    Charger Compact Portable
                                                                                    with Premium
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('51');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('51');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('51');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='111'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                                               target="_self"
                                                                               title="Compact (External Battery Power Bank) with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/12-270x270.jpg"
                                                                                     alt="Compact (External Battery Power Bank) with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                                                   title="Compact (External Battery Power Bank) with Premium"
                                                                                   target="_self">
                                                                                    Compact (External
                                                                                    Battery Power Bank) wi..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('111');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('111');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('111');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='75'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery) T21">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/14-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery) T21">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                                                   title="Compact Portable Charger (External Battery) T21"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('75');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('75');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('75');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='88'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery) T22">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/21-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery) T22">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                                                   title="Compact Portable Charger (External Battery) T22"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('88');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('88');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('88');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='58'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                               target="_self"
                                                                               title="Computer Science saepe eveniet ut et volu redae">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                                                     alt="Computer Science saepe eveniet ut et volu redae">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-2%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                                   title="Computer Science saepe eveniet ut et volu redae"
                                                                                   target="_self">
                                                                                    Computer Science saepe
                                                                                    eveniet ut et vol..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$119.60</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('58');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('58');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('58');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='105'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                               target="_self"
                                                                               title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                                     alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium"
                                                                                   target="_self">
                                                                                    Lorem Ipsum dolor at
                                                                                    vero eos et iusto o..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('105');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('105');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('105');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='110'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=110"
                                                                               target="_self"
                                                                               title="Mammo Diablo except to obtain some advan from">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/6-270x270.jpg"
                                                                                     alt="Mammo Diablo except to obtain some advan from">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=110"
                                                                                   title="Mammo Diablo except to obtain some advan from"
                                                                                   target="_self">
                                                                                    Mammo Diablo except to
                                                                                    obtain some advan..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('110');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('110');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('110');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='78'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                               target="_self"
                                                                               title="Portable  Compact Charger (External Battery) t45">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                                                     alt="Portable  Compact Charger (External Battery) t45">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                                   title="Portable  Compact Charger (External Battery) t45"
                                                                                   target="_self">
                                                                                    Portable Compact Charger
                                                                                    (External Batt..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('78');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('78');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('78');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='40'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=40"
                                                                               target="_self"
                                                                               title="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/28-270x270.jpg"
                                                                                     alt="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=40"
                                                                                   title="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)"
                                                                                   target="_self">
                                                                                    LG 29UC97-S 29"(21:9)
                                                                                    FHD IPS LED 2560X..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $123.20

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('40');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('40');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('40');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='66'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (Power Bank) with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                                                     alt="Compact Portable Charger (Power Bank) with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-70%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                                   title="Compact Portable Charger (Power Bank) with Premium"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (Power Bank) wi..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$241.99</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('66');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('66');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('66');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='64'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=64"
                                                                               target="_self"
                                                                               title="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 ">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/5-270x270.jpg"
                                                                                     alt="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 ">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=64"
                                                                                   title="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 "
                                                                                   target="_self">
                                                                                    SamSung 23UC97-S
                                                                                    29"(21:9) FHD IPS LED ..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $337.99

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('64');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('64');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('64');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            var $tag_id = $('#so_listing_tabs_246'),
                                                                parent_active = $('.items-category-28', $tag_id),
                                                                total_product = parent_active.data('total'),
                                                                tab_active = $('.ltabs-items-inner', parent_active),
                                                                nb_column0 = 4,
                                                                nb_column1 = 3,
                                                                nb_column2 = 2,
                                                                nb_column3 = 2,
                                                                nb_column4 = 1;
                                                            tab_active.owlCarousel2({
                                                                rtl: false,
                                                                nav: true,
                                                                dots: false,
                                                                margin: 0,
                                                                loop: false,
                                                                autoplay: false,
                                                                autoplayHoverPause: false,
                                                                autoplayTimeout: 5000,
                                                                autoplaySpeed: 1000,
                                                                mouseDrag: true,
                                                                touchDrag: true,
                                                                navRewind: true,
                                                                navText: ['', ''],
                                                                responsive: {
                                                                    0: {
                                                                        items: nb_column4,
                                                                        nav: total_product <= nb_column4 ? false : true,
                                                                    },
                                                                    480: {
                                                                        items: nb_column3,
                                                                        nav: total_product <= nb_column3 ? false : true,
                                                                    },
                                                                    768: {
                                                                        items: nb_column2,
                                                                        nav: total_product <= nb_column2 ? false : true,
                                                                    },
                                                                    992: {
                                                                        items: nb_column1,
                                                                        nav: total_product <= nb_column1 ? false : true,
                                                                    },
                                                                    1200: {
                                                                        items: nb_column0,
                                                                        nav: total_product <= nb_column0 ? false : true,
                                                                    },
                                                                    1400: {
                                                                        items: 5,

                                                                    }
                                                                }
                                                            });
                                                        });
                                                    </script>


                                                </div>
                                                <div class="ltabs-items  items-category-30" data-total="16">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-89" data-total="14">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-90" data-total="12">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-77" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-79" data-total="13">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-80" data-total="14">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-62" data-total="16">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            ;
                                            (function (element) {
                                                var $element = $(element),
                                                    $tab = $('.ltabs-tab', $element),
                                                    $tab_label = $('.ltabs-tab-label', $tab),
                                                    $tabs = $('.ltabs-tabs', $element),
                                                    ajax_url = $tabs.parents('.ltabs-tabs-container').attr('data-ajaxurl'),
                                                    effect = $tabs.parents('.ltabs-tabs-container').attr('data-effect'),
                                                    delay = $tabs.parents('.ltabs-tabs-container').attr('data-delay'),
                                                    duration = $tabs.parents('.ltabs-tabs-container').attr('data-duration'),
                                                    type_source = $tabs.parents('.ltabs-tabs-container').attr('data-type_source'),
                                                    $items_content = $('.ltabs-items', $element),
                                                    $items_inner = $('.ltabs-items-inner', $items_content),
                                                    $items_first_active = $('.ltabs-items-selected', $element),
                                                    $load_more = $('.ltabs-loadmore', $element),
                                                    $btn_loadmore = $('.ltabs-loadmore-btn', $load_more),
                                                    $select_box = $('.ltabs-selectbox', $element),
                                                    $tab_label_select = $('.ltabs-tab-selected', $element),
                                                    setting = 'a:73:{s:6:"action";s:9:"save_edit";s:4:"name";s:32:"Home3 - Digital &amp; Electronic";s:18:"module_description";a:2:{i:2;a:1:{s:9:"head_name";s:24:"Digital &amp; Electronic";}i:1;a:1:{s:9:"head_name";s:24:"Digital &amp; Electronic";}}s:9:"head_name";s:24:"Digital &amp; Electronic";s:17:"disp_title_module";s:1:"1";s:6:"status";s:1:"1";s:12:"store_layout";s:6:"style5";s:12:"class_suffix";s:23:"home3_listingtab_style2";s:16:"item_link_target";s:5:"_self";s:10:"nb_column0";s:1:"4";s:10:"nb_column1";s:1:"3";s:10:"nb_column2";s:1:"2";s:10:"nb_column3";s:1:"2";s:10:"nb_column4";s:1:"1";s:9:"type_show";s:6:"slider";s:6:"nb_row";s:1:"2";s:11:"type_source";s:1:"0";s:8:"category";a:8:{i:0;s:2:"28";i:1;s:2:"89";i:2;s:2:"90";i:3;s:2:"77";i:4;s:2:"79";i:5;s:2:"80";i:6;s:2:"30";i:7;s:2:"62";}s:14:"child_category";s:1:"1";s:14:"category_depth";s:1:"1";s:12:"product_sort";s:7:"p.price";s:16:"product_ordering";s:3:"ASC";s:12:"source_limit";s:2:"12";s:13:"catid_preload";s:2:"28";s:17:"field_product_tab";s:0:"";s:15:"tab_all_display";s:1:"0";s:18:"tab_max_characters";s:2:"50";s:16:"tab_icon_display";s:1:"1";s:12:"cat_order_by";s:4:"name";s:15:"imgcfgcat_width";s:2:"60";s:16:"imgcfgcat_height";s:2:"60";s:13:"display_title";s:1:"1";s:15:"title_maxlength";s:2:"40";s:19:"display_description";s:1:"0";s:21:"description_maxlength";s:3:"100";s:13:"display_price";s:1:"1";s:19:"display_add_to_cart";s:1:"1";s:16:"display_wishlist";s:1:"1";s:15:"display_compare";s:1:"1";s:14:"display_rating";s:1:"0";s:12:"display_sale";s:1:"1";s:11:"display_new";s:1:"1";s:8:"date_day";s:3:"227";s:17:"product_image_num";s:1:"1";s:13:"product_image";s:1:"1";s:22:"product_get_image_data";s:1:"1";s:23:"product_get_image_image";s:1:"1";s:5:"width";s:3:"270";s:6:"height";s:3:"270";s:24:"product_placeholder_path";s:11:"http://opencart.opencartworks.com/themes/so_topdeal3/nophoto.png";s:20:"display_banner_image";s:1:"0";s:12:"banner_image";s:12:"http://opencart.opencartworks.com/themes/so_topdeal3/no_image.png";s:12:"banner_width";s:3:"150";s:13:"banner_height";s:3:"250";s:16:"banner_image_url";s:0:"";s:6:"effect";s:4:"none";s:8:"duration";s:3:"800";s:5:"delay";s:3:"500";s:8:"autoplay";s:1:"0";s:11:"display_nav";s:1:"1";s:12:"display_dots";s:1:"0";s:12:"display_loop";s:1:"0";s:9:"touchdrag";s:1:"1";s:9:"mousedrag";s:1:"1";s:10:"pausehover";s:1:"0";s:15:"autoplayTimeout";s:4:"5000";s:13:"autoplaySpeed";s:4:"1000";s:8:"pre_text";s:0:"";s:9:"post_text";s:0:"";s:9:"use_cache";s:1:"0";s:10:"cache_time";s:4:"3600";s:8:"moduleid";s:3:"246";s:5:"start";i:0;}',
                                                    type_show = 'slider';

                                                enableSelectBoxes();

                                                function enableSelectBoxes() {
                                                    $tab_wrap = $('.ltabs-tabs-wrap', $element),
                                                        $tab_label_select.html($('.ltabs-tab', $element).filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    if ($(window).innerWidth() <= 479) {
                                                        $tab_wrap.addClass('ltabs-selectbox');
                                                    } else {
                                                        $tab_wrap.removeClass('ltabs-selectbox');
                                                    }
                                                }

                                                $('span.ltabs-tab-selected, span.ltabs-tab-arrow', $element).click(function () {
                                                    if ($('.ltabs-tabs', $element).hasClass('ltabs-open')) {
                                                        $('.ltabs-tabs', $element).removeClass('ltabs-open');
                                                    } else {
                                                        $('.ltabs-tabs', $element).addClass('ltabs-open');
                                                    }
                                                });

                                                $(window).resize(function () {
                                                    if ($(window).innerWidth() <= 479) {
                                                        $('.ltabs-tabs-wrap', $element).addClass('ltabs-selectbox');
                                                    } else {
                                                        $('.ltabs-tabs-wrap', $element).removeClass('ltabs-selectbox');
                                                    }
                                                });

                                                function showAnimateItems(el) {
                                                    var $_items = $('.new-ltabs-item', el), nub = 0;
                                                    $('.ltabs-loadmore-btn', el).fadeOut('fast');
                                                    $_items.each(function (i) {
                                                        nub++;
                                                        switch (effect) {
                                                            case 'none' :
                                                                $(this).css({
                                                                    'opacity': '1',
                                                                    'filter': 'alpha(opacity = 100)'
                                                                });
                                                                break;
                                                            default:
                                                                animatesItems($(this), nub * delay, i, el);
                                                        }
                                                        if (i == $_items.length - 1) {
                                                            $('.ltabs-loadmore-btn', el).fadeIn(3000);
                                                        }
                                                        $(this).removeClass('new-ltabs-item');
                                                    });
                                                }

                                                function animatesItems($this, fdelay, i, el) {
                                                    var $_items = $('.ltabs-item', el);
                                                    $this.stop(true, true).attr("style",
                                                        "-webkit-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation:" + effect + " " + duration + "ms;"
                                                        + "-o-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation-delay:" + fdelay + "ms;"
                                                        + "-webkit-animation-delay:" + fdelay + "ms;"
                                                        + "-o-animation-delay:" + fdelay + "ms;"
                                                        + "animation-delay:" + fdelay + "ms;").delay(fdelay).animate({
                                                        opacity: 1,
                                                        filter: 'alpha(opacity = 100)'
                                                    }, {
                                                        delay: 1000
                                                    });
                                                    if (i == ($_items.length - 1)) {
                                                        $(".ltabs-items-inner").addClass("play");
                                                    }
                                                }

                                                if (type_show == 'loadmore') {
                                                    showAnimateItems($items_first_active);
                                                }
                                                $tab.on('click.ltabs-tab', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('tab-sel')) return false;
                                                    if ($this.parents('.ltabs-tabs').hasClass('ltabs-open')) {
                                                        $this.parents('.ltabs-tabs').removeClass('ltabs-open');
                                                    }
                                                    $tab.removeClass('tab-sel');
                                                    $this.addClass('tab-sel');
                                                    var items_active = $this.attr('data-active-content-l');
                                                    var _items_active = $(items_active, $element);
                                                    $items_content.removeClass('ltabs-items-selected');
                                                    _items_active.addClass('ltabs-items-selected');
                                                    $tab_label_select.html($tab.filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    var $loading = $('.ltabs-loading', _items_active);
                                                    var loaded = _items_active.hasClass('ltabs-items-loaded');
                                                    type_show = $tabs.parents('.ltabs-tabs-container').attr('data-type_show');
                                                    if (!loaded && !_items_active.hasClass('ltabs-process')) {
                                                        _items_active.addClass('ltabs-process');
                                                        var category_id = $this.attr('data-category-id');
                                                        $loading.show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: ajax_url,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: 0,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 246
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $('.ltabs-loading', _items_active).replaceWith(data.items_markup);
                                                                    _items_active.addClass('ltabs-items-loaded').removeClass('ltabs-process');
                                                                    $loading.remove();
                                                                    if (type_show != 'slider') {
                                                                        showAnimateItems(_items_active);
                                                                    }
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }

                                                            },
                                                            dataType: 'json'
                                                        });

                                                    } else {
                                                        if (type_show == 'loadmore') {
                                                            $('.ltabs-item', $items_content).removeAttr('style').addClass('new-ltabs-item');
                                                            showAnimateItems(_items_active);
                                                        } else {
                                                            var owl = $('.owl2-carousel', _items_active);
                                                            owl = owl.data('owlCarousel2');
                                                            if (typeof owl !== 'undefined') {
                                                                owl.onResize();
                                                            }
                                                        }
                                                    }
                                                });

                                                function updateStatus($el) {
                                                    $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    var countitem = $('.ltabs-item', $el).length;
                                                    $('.ltabs-image-loading', $el).css({display: 'none'});
                                                    $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_start', countitem);
                                                    var rl_total = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_total');
                                                    var rl_load = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_load');
                                                    var rl_allready = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_allready');

                                                    if (countitem >= rl_total) {
                                                        $('.ltabs-loadmore-btn', $el).addClass('loaded');
                                                        $('.ltabs-image-loading', $el).css({display: 'none'});
                                                        $('.ltabs-loadmore-btn', $el).attr('data-label', rl_allready);
                                                        $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    }
                                                }

                                                $btn_loadmore.on('click.loadmore', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('loaded') || $this.hasClass('loading')) {
                                                        return false;
                                                    } else {
                                                        $this.addClass('loading');
                                                        $('.ltabs-image-loading', $this).css({display: 'inline-block'});
                                                        var rl_start = $this.parent().attr('data-rl_start'),
                                                            rl_ajaxurl = $this.parent().attr('data-ajaxurl'),
                                                            effect = $this.parent().attr('data-effect'),
                                                            category_id = $this.parent().attr('data-categoryid'),
                                                            items_active = $this.parent().attr('data-active-content');

                                                        var _items_active = $(items_active, $element);

                                                        $.ajax({
                                                            type: 'POST',
                                                            url: rl_ajaxurl,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: rl_start,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 246
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $($(data.items_markup).html()).insertAfter($('.ltabs-item', _items_active).nextAll().last());
                                                                    $('.ltabs-image-loading', $this).css({display: 'none'});
                                                                    showAnimateItems(_items_active);
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }
                                                            }, dataType: 'json'
                                                        });
                                                    }
                                                    return false;
                                                });
                                            })('#so_listing_tabs_246');
                                        });
                                        //]]>
                                    </script>
                                </div>
                            </div> <!-- /.modcontent-->

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_2xqd block">
                        <div class="banner-layout-5 row clearfix">
                            <div class="banner-22 col-sm-4  banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 22">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner22.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>
                            <div class="banner-23 col-sm-4 banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 23">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner23.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>

                            <div class="banner-24 col-sm-4  banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 24">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner24.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_u7mo  block">

                <div class="module so-extraslider-ltr home3_extra_style3">

                    <h3 class="modtitle">Feature Items</h3>


                    <div class="modcontent">


                        <div id="so_extra_slider_20522393041573891479"
                             class="so-extraslider buttom-type1 preset00-6 preset01-5 preset02-3 preset03-2 preset04-1 button-type1">
                            <div class="box-banner">
                                <div class="banners">
                                </div>
                            </div>
                            <!-- Begin extraslider-inner -->
                            <div class="extraslider-inner products-list grid" data-effect="none">

                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='104'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                       target="_self"
                                                       title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/25-270x270.jpg"
                                                             alt="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-82% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                           target="_self"
                                                           title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2) ">
                                                            Toshiba Pro 21&quot;(21:9) FHD IPS LED 1920X1080
                                                            HDMI(2)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$337.99 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('104');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('104');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('104');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='66'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                       target="_self"
                                                       title="Compact Portable Charger (Power Bank) with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                             alt="Compact Portable Charger (Power Bank) with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-70% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                           target="_self"
                                                           title="Compact Portable Charger (Power Bank) with Premium ">
                                                            Compact Portable Charger (Power Bank) with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$241.99 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('66');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('66');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('66');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='105'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                       target="_self"
                                                       title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                             alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-40% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                           target="_self"
                                                           title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium ">
                                                            Lorem Ipsum dolor at vero eos et iusto odi with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('105');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('105');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('105');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='30'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                       target="_self"
                                                       title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                             alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                           target="_self"
                                                           title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus ">
                                                            Magnetic Air Vent Phone Holder for iPhone 7 / 7
                                                            Plus
                                                        </a>
                                                    </h4>


                                                    <div class="rating">

                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('30');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('30');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('30');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='51'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                       target="_self"
                                                       title="Charger  Compact Portable with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                             alt="Charger  Compact Portable with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                           target="_self"
                                                           title="Charger  Compact Portable with Premium ">
                                                            Charger Compact Portable with Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('51');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('51');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('51');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='103'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery)">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) ">
                                                            Compact Portable Charger (External Battery)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$98.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('103');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('103');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('103');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='88'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery) T22">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/21-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery) T22">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) T22 ">
                                                            Compact Portable Charger (External Battery) T22
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('88');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('88');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('88');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='78'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                       target="_self"
                                                       title="Portable  Compact Charger (External Battery) t45">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                             alt="Portable  Compact Charger (External Battery) t45">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                           target="_self"
                                                           title="Portable  Compact Charger (External Battery) t45 ">
                                                            Portable Compact Charger (External Battery) t45
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('78');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('78');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('78');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='86'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                       target="_self"
                                                       title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/29-270x270.jpg"
                                                             alt="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-11% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                           target="_self"
                                                           title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly) ">
                                                            Seneo PA046 Fast Charger Wireless
                                                            Sleep-Friendly)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('86');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('86');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('86');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='58'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                       target="_self"
                                                       title="Computer Science saepe eveniet ut et volu redae">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                             alt="Computer Science saepe eveniet ut et volu redae">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-2% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                           target="_self"
                                                           title="Computer Science saepe eveniet ut et volu redae ">
                                                            Computer Science saepe eveniet ut et volu redae
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$119.60 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('58');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('58');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('58');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='111'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                       target="_self"
                                                       title="Compact (External Battery Power Bank) with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/12-270x270.jpg"
                                                             alt="Compact (External Battery Power Bank) with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                           target="_self"
                                                           title="Compact (External Battery Power Bank) with Premium ">
                                                            Compact (External Battery Power Bank) with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$122.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('111');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('111');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('111');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='75'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery) T21">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/14-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery) T21">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) T21 ">
                                                            Compact Portable Charger (External Battery) T21
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$122.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('75');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('75');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('75');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                            </div>
                            <!--End extraslider-inner -->

                            <script type="text/javascript">
                                //<![CDATA[
                                jQuery(document).ready(function ($) {
                                    (function (element) {
                                        var $element = $(element),
                                            $extraslider = $(".extraslider-inner", $element),
                                            _delay = 500,
                                            _duration = 800,
                                            _effect = 'none ';

                                        $extraslider.on("initialized.owl.carousel2", function () {
                                            var $item_active = $(".owl2-item.active", $element);
                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {
                                                var $item = $(".owl2-item", $element);
                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                                            }


                                            $(".owl2-controls", $element).insertBefore($extraslider);
                                            $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                        });

                                        $extraslider.owlCarousel2({
                                            rtl: false,
                                            margin: 0,
                                            slideBy: 1,
                                            autoplay: 0,
                                            autoplayHoverPause: 0,
                                            autoplayTimeout: 0,
                                            autoplaySpeed: 1000,
                                            startPosition: 0,
                                            mouseDrag: 1,
                                            touchDrag: 1,
                                            autoWidth: false,
                                            responsive: {
                                                0: {items: 1},
                                                480: {items: 2},
                                                768: {items: 3},
                                                1200: {items: 5},
                                                1400: {items: 6}
                                            },
                                            dotClass: "owl2-dot",
                                            dotsClass: "owl2-dots",
                                            dots: false,
                                            dotsSpeed: 500,
                                            nav: false,
                                            loop: false,
                                            navSpeed: 500,
                                            navText: ["&#171 ", "&#187 "],
                                            navClass: ["owl2-prev", "owl2-next"]

                                        });

                                        $extraslider.on("translate.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            _UngetAnimate($item_active);
                                            _getAnimate($item_active);
                                        });

                                        $extraslider.on("translated.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            var $item = $(".owl2-item", $element);

                                            _UngetAnimate($item);

                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {

                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});

                                            }
                                        });

                                        function _getAnimate($el) {
                                            if (_effect == "none") return;
                                            //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                            $extraslider.removeClass("extra-animate");
                                            $el.each(function (i) {
                                                var $_el = $(this);
                                                $(this).css({
                                                    "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                    "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                    "-o-animation": _effect + " " + _duration + "ms ease both",
                                                    "animation": _effect + " " + _duration + "ms ease both",
                                                    "-webkit-animation-delay": +i * _delay + "ms",
                                                    "-moz-animation-delay": +i * _delay + "ms",
                                                    "-o-animation-delay": +i * _delay + "ms",
                                                    "animation-delay": +i * _delay + "ms",
                                                    "opacity": 1
                                                }).animate({
                                                    opacity: 1
                                                });

                                                if (i == $el.size() - 1) {
                                                    $extraslider.addClass("extra-animate");
                                                }
                                            });
                                        }

                                        function _UngetAnimate($el) {
                                            $el.each(function (i) {
                                                $(this).css({
                                                    "animation": "",
                                                    "-webkit-animation": "",
                                                    "-moz-animation": "",
                                                    "-o-animation": "",
                                                    "opacity": 1
                                                });
                                            });
                                        }

                                    })("#so_extra_slider_20522393041573891479 ");
                                });
                                //]]>
                            </script>

                        </div>

                    </div>

                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_g1nr  block">
                <div class="row">
                    <div class="banner-25 col-sm-6 banners">
                        <div>
                            <a class="bn-shadow" href="#" title="Banner 25">
                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner25.jpg"
                                     alt="Static Image">
                            </a>
                        </div>
                    </div>
                    <div class="banner-26 col-sm-6  banners">
                        <div>
                            <a class="bn-shadow" href="#" title="Banner 26">
                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner26.jpg"
                                     alt="Static Image">
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</section><section id="" class="section_1_h2 ">
    <div class="container page-builder-ltr">
        <div class="row row_o7yw  row-style ">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col_q4w9  menu_vertical">

                <div class="row row_u7kh  row-style ">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_1x14">
                        <div class="qr-code-block block">
                            <div class="item-image"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/qr.png"
                                    alt="" width="65" height="65"></div>
                            <div class="item-content">
                                <h4>Enjoy Convenient Order Tracking</h4>
                                <p><a href="#">Scan to download app</a></p>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_35kw block">

                        <div class="module so-extraslider-ltr home3_extra bn-shadow">

                            <h3 class="modtitle">Special Items</h3>


                            <div class="modcontent">


                                <div id="so_extra_slider_3849357241573891478"
                                     class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-1 preset04-1 button-type1">
                                    <div class="box-banner">
                                        <div class="banners">
                                        </div>
                                    </div>
                                    <!-- Begin extraslider-inner -->
                                    <div class="extraslider-inner products-list grid" data-effect="none">

                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='98'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=98"
                                                               target="_blank"
                                                               title="Pariatur beef desktop publishing package..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/5-270x270.jpg"
                                                                     alt="Pariatur beef desktop publishing packages monst">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=98"
                                                                   target="_blank"
                                                                   title="Pariatur beef desktop publishing packages monst ">
                                                                    Pariatur beef desktop publishing
                                                                    package..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$56.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$62.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='29'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                               target="_blank"
                                                               title="Est Officia Including Shoes Beautiful Pi..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/15-270x270.png"
                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                   target="_blank"
                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz ">
                                                                    Est Officia Including Shoes Beautiful
                                                                    Pi..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$337.99 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='42'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                               target="_blank"
                                                               title="Est Officia Including Shoes Beautiful Pi..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/24-270x270.png"
                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                   target="_blank"
                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz ">
                                                                    Est Officia Including Shoes Beautiful
                                                                    Pi..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='82'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=82"
                                                               target="_blank"
                                                               title="Maciti Aliqua occur that pleasures have ..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/21-270x270.png"
                                                                     alt="Maciti Aliqua occur that pleasures have a lotem">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=82"
                                                                   target="_blank"
                                                                   title="Maciti Aliqua occur that pleasures have a lotem ">
                                                                    Maciti Aliqua occur that pleasures have
                                                                    ..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='28'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=28"
                                                               target="_blank"
                                                               title="Amazing Yoga Sport Poses Most  People Wo..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/10-270x270.jpg"
                                                                     alt="Amazing Yoga Sport Poses Most  People Wouldn't Dream ">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=28"
                                                                   target="_blank"
                                                                   title="Amazing Yoga Sport Poses Most  People Wouldn't Dream  ">
                                                                    Amazing Yoga Sport Poses Most People
                                                                    Wo..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='57'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=57"
                                                               target="_blank"
                                                               title="Men Winter Down Coat Jackets  Women Thic..">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/10-270x270.jpg"
                                                                     alt="Men Winter Down Coat Jackets  Women Thicken">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=57"
                                                                   target="_blank"
                                                                   title="Men Winter Down Coat Jackets  Women Thicken ">
                                                                    Men Winter Down Coat Jackets Women
                                                                    Thic..
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                    </div>
                                    <!--End extraslider-inner -->

                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            (function (element) {
                                                var $element = $(element),
                                                    $extraslider = $(".extraslider-inner", $element),
                                                    _delay = 500,
                                                    _duration = 800,
                                                    _effect = 'none ';

                                                $extraslider.on("initialized.owl.carousel2", function () {
                                                    var $item_active = $(".owl2-item.active", $element);
                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {
                                                        var $item = $(".owl2-item", $element);
                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });
                                                    }


                                                    $(".owl2-controls", $element).insertBefore($extraslider);
                                                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                                });

                                                $extraslider.owlCarousel2({
                                                    rtl: false,
                                                    margin: 0,
                                                    slideBy: 1,
                                                    autoplay: 0,
                                                    autoplayHoverPause: 0,
                                                    autoplayTimeout: 0,
                                                    autoplaySpeed: 1000,
                                                    startPosition: 0,
                                                    mouseDrag: 1,
                                                    touchDrag: 1,
                                                    autoWidth: false,
                                                    responsive: {
                                                        0: {items: 1},
                                                        480: {items: 1},
                                                        768: {items: 1},
                                                        1200: {items: 1},
                                                        1400: {items: 1}
                                                    },
                                                    dotClass: "owl2-dot",
                                                    dotsClass: "owl2-dots",
                                                    dots: false,
                                                    dotsSpeed: 500,
                                                    nav: false,
                                                    loop: false,
                                                    navSpeed: 500,
                                                    navText: ["&#171 ", "&#187 "],
                                                    navClass: ["owl2-prev", "owl2-next"]

                                                });

                                                $extraslider.on("translate.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    _UngetAnimate($item_active);
                                                    _getAnimate($item_active);
                                                });

                                                $extraslider.on("translated.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    var $item = $(".owl2-item", $element);

                                                    _UngetAnimate($item);

                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {

                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });

                                                    }
                                                });

                                                function _getAnimate($el) {
                                                    if (_effect == "none") return;
                                                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                                    $extraslider.removeClass("extra-animate");
                                                    $el.each(function (i) {
                                                        var $_el = $(this);
                                                        $(this).css({
                                                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                            "-o-animation": _effect + " " + _duration + "ms ease both",
                                                            "animation": _effect + " " + _duration + "ms ease both",
                                                            "-webkit-animation-delay": +i * _delay + "ms",
                                                            "-moz-animation-delay": +i * _delay + "ms",
                                                            "-o-animation-delay": +i * _delay + "ms",
                                                            "animation-delay": +i * _delay + "ms",
                                                            "opacity": 1
                                                        }).animate({
                                                            opacity: 1
                                                        });

                                                        if (i == $el.size() - 1) {
                                                            $extraslider.addClass("extra-animate");
                                                        }
                                                    });
                                                }

                                                function _UngetAnimate($el) {
                                                    $el.each(function (i) {
                                                        $(this).css({
                                                            "animation": "",
                                                            "-webkit-animation": "",
                                                            "-moz-animation": "",
                                                            "-o-animation": "",
                                                            "opacity": 1
                                                        });
                                                    });
                                                }

                                            })("#so_extra_slider_3849357241573891478 ");
                                        });
                                        //]]>
                                    </script>

                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_l3lb block hidden-xs">
                        <div class="banner-layout-3 bt-1 clearfix banners">
                            <div class="">
                                <a class="bn-shadow" href="#" title="Banner 1">
                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/01.jpg"
                                         alt="Static Image">
                                </a>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_z8xz block">

                        <div class="module so-extraslider-ltr home3_extra_style2 bn-shadow">

                            <h3 class="modtitle">Recommend Items</h3>


                            <div class="modcontent">


                                <div id="so_extra_slider_15914538661573891478"
                                     class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-2 preset04-1 button-type1">
                                    <div class="box-banner">
                                        <div class="banners">
                                        </div>
                                    </div>
                                    <!-- Begin extraslider-inner -->
                                    <div class="extraslider-inner products-list grid" data-effect="none">

                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='104'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                               target="_self"
                                                               title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/25-270x270.jpg"
                                                                     alt="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-82% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                                   target="_self"
                                                                   title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2) ">
                                                                    Toshiba Pro 21&quot;(21:9) FHD IPS LED
                                                                    1920X1080 HDMI(2)
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$337.99 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('104');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('104');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('104');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='66'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                               target="_self"
                                                               title="Compact Portable Charger (Power Bank) with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                                     alt="Compact Portable Charger (Power Bank) with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-70% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                   target="_self"
                                                                   title="Compact Portable Charger (Power Bank) with Premium ">
                                                                    Compact Portable Charger (Power Bank)
                                                                    with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$241.99 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('66');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('66');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('66');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='105'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                               target="_self"
                                                               title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                     alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-40% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium ">
                                                                    Lorem Ipsum dolor at vero eos et iusto
                                                                    odi with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('105');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('105');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('105');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                        <div class="item ">

                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='30'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                               target="_self"
                                                               title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                                     alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-20% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus ">
                                                                    Magnetic Air Vent Phone Holder for
                                                                    iPhone 7 / 7 Plus
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('30');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('30');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('30');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='51'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                               target="_self"
                                                               title="Charger  Compact Portable with Premium">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                     alt="Charger  Compact Portable with Premium">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">

                                                            <span class="label-product label-sale">-20% </span>


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium ">
                                                                    Charger Compact Portable with Premium
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

                                                                <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                                <span class="price-old">$122.00 </span>&nbsp;


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('51');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('51');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('51');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                            <div class="item-wrap product-layout style1 ">
                                                <div class="product-item-container">
                                                    <div class="left-block ">
                                                        <div class="product-image-container so-quickview">
                                                            <a class="lt-image"
                                                               data-product='103'
                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                               target="_self"
                                                               title="Compact Portable Charger (External Battery)">

                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                                     alt="Compact Portable Charger (External Battery)">
                                                            </a>

                                                        </div>
                                                        <div class="box-label">


                                                        </div>

                                                    </div>


                                                    <div class="right-block">
                                                        <div class="caption">

                                                            <h4 class="font-ct">
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                   target="_self"
                                                                   title="Compact Portable Charger (External Battery) ">
                                                                    Compact Portable Charger (External
                                                                    Battery)
                                                                </a>
                                                            </h4>


                                                            <div class="content_price price font-ct">

														<span class="price product-price">
															$98.00
														</span>


                                                            </div>

                                                        </div>


                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('103');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('103');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('103');"><i
                                                                    class="fa fa-exchange"></i></button>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- End item-wrap-inner -->
                                            </div>
                                            <!-- End item-wrap -->


                                        </div>


                                    </div>
                                    <!--End extraslider-inner -->

                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            (function (element) {
                                                var $element = $(element),
                                                    $extraslider = $(".extraslider-inner", $element),
                                                    _delay = 500,
                                                    _duration = 800,
                                                    _effect = 'none ';

                                                $extraslider.on("initialized.owl.carousel2", function () {
                                                    var $item_active = $(".owl2-item.active", $element);
                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {
                                                        var $item = $(".owl2-item", $element);
                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });
                                                    }


                                                    $(".owl2-controls", $element).insertBefore($extraslider);
                                                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                                });

                                                $extraslider.owlCarousel2({
                                                    rtl: false,
                                                    margin: 0,
                                                    slideBy: 1,
                                                    autoplay: 0,
                                                    autoplayHoverPause: 0,
                                                    autoplayTimeout: 0,
                                                    autoplaySpeed: 1000,
                                                    startPosition: 0,
                                                    mouseDrag: 1,
                                                    touchDrag: 1,
                                                    autoWidth: false,
                                                    responsive: {
                                                        0: {items: 1},
                                                        480: {items: 2},
                                                        768: {items: 1},
                                                        1200: {items: 1},
                                                        1400: {items: 1}
                                                    },
                                                    dotClass: "owl2-dot",
                                                    dotsClass: "owl2-dots",
                                                    dots: false,
                                                    dotsSpeed: 500,
                                                    nav: false,
                                                    loop: false,
                                                    navSpeed: 500,
                                                    navText: ["&#171 ", "&#187 "],
                                                    navClass: ["owl2-prev", "owl2-next"]

                                                });

                                                $extraslider.on("translate.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    _UngetAnimate($item_active);
                                                    _getAnimate($item_active);
                                                });

                                                $extraslider.on("translated.owl.carousel2", function (e) {


                                                    var $item_active = $(".owl2-item.active", $element);
                                                    var $item = $(".owl2-item", $element);

                                                    _UngetAnimate($item);

                                                    if ($item_active.length > 1 && _effect != "none") {
                                                        _getAnimate($item_active);
                                                    } else {

                                                        $item.css({
                                                            "opacity": 1,
                                                            "filter": "alpha(opacity = 100)"
                                                        });

                                                    }
                                                });

                                                function _getAnimate($el) {
                                                    if (_effect == "none") return;
                                                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                                    $extraslider.removeClass("extra-animate");
                                                    $el.each(function (i) {
                                                        var $_el = $(this);
                                                        $(this).css({
                                                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                            "-o-animation": _effect + " " + _duration + "ms ease both",
                                                            "animation": _effect + " " + _duration + "ms ease both",
                                                            "-webkit-animation-delay": +i * _delay + "ms",
                                                            "-moz-animation-delay": +i * _delay + "ms",
                                                            "-o-animation-delay": +i * _delay + "ms",
                                                            "animation-delay": +i * _delay + "ms",
                                                            "opacity": 1
                                                        }).animate({
                                                            opacity: 1
                                                        });

                                                        if (i == $el.size() - 1) {
                                                            $extraslider.addClass("extra-animate");
                                                        }
                                                    });
                                                }

                                                function _UngetAnimate($el) {
                                                    $el.each(function (i) {
                                                        $(this).css({
                                                            "animation": "",
                                                            "-webkit-animation": "",
                                                            "-moz-animation": "",
                                                            "-o-animation": "",
                                                            "opacity": 1
                                                        });
                                                    });
                                                }

                                            })("#so_extra_slider_15914538661573891478 ");
                                        });
                                        //]]>
                                    </script>

                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_ydxt block">
                        <div class="block-testimonial bn-shadow">
                            <div class="testimonial-items contentslider" data-rtl="no" data-loop="no"
                                 data-autoplay="yes" data-autoheight="no" data-autowidth="no" data-delay="4"
                                 data-speed="0.6" data-margin="0" data-items_column0="1"
                                 data-items_column1="1" data-items_column2="1" data-items_column3="1"
                                 data-items_column4="1" data-arrows="no" data-pagination="yes"
                                 data-lazyload="yes" data-hoverpause="yes">
                                <div class="item">
                                    <div class="text">
                                        <div class="t">Lorem Khaled Ipsum is a major key to success. It’s on
                                            you how you want to live your life. Everyone has a choice. I
                                            pick my choice, squeaky clean. Always remember in the jungle
                                            there’s a lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-2.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">Sharon Stone</div>
                                    <div class="job">Acc - Hollywood</div>
                                </div>
                                <div class="item">
                                    <div class="text">
                                        <div class="t">
                                            Khaled Lorem Ipsum is a major key to success. It’s on you how
                                            you want to live your life. Everyone has a choice. I pick my
                                            choice, squeaky clean. Always remember in the jungle there’s a
                                            lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-1.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">David Beckham</div>
                                    <div class="job">CE0 - Magentech</div>
                                </div>
                                <div class="item">
                                    <div class="text">
                                        <div class="t">
                                            Lorem Khaled Ipsum is a major key to success. It’s on you how
                                            you want to live your life. Everyone has a choice. I pick my
                                            choice, squeaky clean. Always remember in the jungle there’s a
                                            lot of they in there
                                        </div>
                                    </div>
                                    <div class="img"><img
                                            src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home3/user-3.jpg"
                                            alt="Static Image"></div>
                                    <div class="name">Johny Walker</div>
                                    <div class="job">Manager - United</div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 col_2w6x  slider_container">

                <div class="row row_w6so  row-style ">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_6iia block">
                        <script>
                            //<![CDATA[
                            var listdeal1 = [];
                            //]]>
                        </script>
                        <div class="module so-deals-ltr home3_deal_style2">
                            <div class="head-title">
                                <h2 class="modtitle font-ct"><span>Hot Deal</span></h2>

                                <div class="cslider-item-timer">
                                    <div class="product_time_maxprice"></div>
                                </div>

                                <script type="text/javascript">
                                    //<![CDATA[
                                    listdeal1.push('product_time_maxprice|2020/10/31 00:00:00')
                                    //]]>
                                </script>
                            </div>
                            <div class="modcontent products-list grid">
                                <div id="so_deals_12662999911162019080438"
                                     class="so-deal modcontent products-list grid clearfix preset00-4 preset01-4 preset02-3 preset03-2 preset04-1  button-type1  style2">
                                    <div class="extraslider-inner" data-effect="none">
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='30'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                                         alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                                    Magnetic Air Vent Phone Holder for
                                                                    iPhone 7 / 7 Plus</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('30');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('30');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('30');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='51'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                         alt="Charger  Compact Portable with Premium"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                   target="_self"
                                                                   title="Charger  Compact Portable with Premium">Charger
                                                                    Compact Portable with Premium</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('51');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('51');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('51');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='58'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self"
                                                                   title="Computer Science saepe eveniet ut et volu redae">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                                         alt="Computer Science saepe eveniet ut et volu redae"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-2%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                   target="_self"
                                                                   title="Computer Science saepe eveniet ut et volu redae">Computer
                                                                    Science saepe eveniet ut et volu
                                                                    redae</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$119.60</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('58');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('58');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('58');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='105'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                         alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-40%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                   target="_self"
                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">Lorem
                                                                    Ipsum dolor at vero eos et iusto odi
                                                                    with Premium</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$74.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('105');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('105');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('105');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='78'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self"
                                                                   title="Portable  Compact Charger (External Battery) t45">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                                         alt="Portable  Compact Charger (External Battery) t45"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-20%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                   target="_self"
                                                                   title="Portable  Compact Charger (External Battery) t45">Portable
                                                                    Compact Charger (External Battery)
                                                                    t45</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$98.00</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('78');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('78');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('78');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="transition product-layout">
                                                <div class="product-item-container ">
                                                    <div class="left-block ">
                                                        <div class="product-image-container">
                                                            <div class="image so-quickview">
                                                                <a class="lt-image hidden"
                                                                   data-product='86'
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self"
                                                                   title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">

                                                                </a>
                                                                <a class="lt-image"
                                                                   href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self">
                                                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/29-270x270.jpg"
                                                                         alt="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)"
                                                                         class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="box-label">
                                                            <span class="label-product label-sale">-11%</span>

                                                        </div>

                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <h4>
                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                                   target="_self"
                                                                   title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">Seneo
                                                                    PA046 Fast Charger Wireless
                                                                    Sleep-Friendly)</a></h4>
                                                            <div class="rating">
                                                                            <span class="fa fa-stack"><i
                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>

                                                            <div class="price">
                                                                <span class="price-new">$108.80</span>
                                                                <span class="price-old">$122.00</span>
                                                            </div>


                                                        </div>
                                                        <div class="button-group2">
                                                            <button class="bt-cart addToCart" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Cart"
                                                                    onclick="cart.add('86');"><span>Add to Cart</span>
                                                            </button>
                                                            <button class="bt wishlist" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('86');"><i
                                                                    class="fa fa-heart"></i></button>

                                                            <button class="bt compare" type="button"
                                                                    data-toggle="tooltip"
                                                                    title="Compare this Product"
                                                                    onclick="compare.add('86');"><i
                                                                    class="fa fa-exchange"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    jQuery(document).ready(function ($) {
                                        ;
                                        (function (element) {
                                            var $element = $(element),
                                                $extraslider = $('.extraslider-inner', $element),
                                                $featureslider = $('.product-feature', $element),
                                                _delay = 500,
                                                _duration = 800,
                                                _effect = 'none';

                                            $extraslider.on('initialized.owl.carousel2', function () {
                                                var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    var $item = $('.extraslider-inner .owl2-item', $element);
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                                $('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));
                                                $('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');
                                            });

                                            $extraslider.owlCarousel2({
                                                rtl: false,
                                                margin: 1,
                                                slideBy: 1,
                                                autoplay: false,
                                                autoplayHoverPause: 0,
                                                autoplayTimeout: 5000,
                                                autoplaySpeed: 1000,
                                                startPosition: 0,
                                                mouseDrag: true,
                                                touchDrag: true,
                                                autoWidth: false,
                                                responsive: {
                                                    0: {items: 1},
                                                    480: {items: 2},
                                                    768: {items: 3},
                                                    992: {items: 4},
                                                    1200: {items: 4}
                                                },
                                                dotClass: 'owl2-dot',
                                                dotsClass: 'owl2-dots',
                                                dots: false,
                                                dotsSpeed: 500,
                                                nav: false,
                                                loop: true,
                                                navSpeed: 500,
                                                navText: ['&#171;', '&#187;'],
                                                navClass: ['owl2-prev', 'owl2-next']
                                            });

                                            $extraslider.on('translated.owl.carousel2', function (e) {
                                                var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                                                var $item = $('.extraslider-inner .owl2-item', $element);

                                                _UngetAnimate($item);

                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                            });
                                            /*feature product*/
                                            $featureslider.on('initialized.owl.carousel2', function () {
                                                var $item_active = $('.product-feature .owl2-item.active', $element);
                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    var $item = $('.owl2-item', $element);
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                                $('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));
                                                $('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');
                                            });

                                            $featureslider.owlCarousel2({
                                                rtl: false,
                                                margin: 1,
                                                slideBy: 1,
                                                autoplay: false,
                                                autoplayHoverPause: 0,
                                                autoplayTimeout: 5000,
                                                autoplaySpeed: 1000,
                                                startPosition: 0,
                                                mouseDrag: true,
                                                touchDrag: true,
                                                autoWidth: false,
                                                responsive: {
                                                    0: {items: 1},
                                                    480: {items: 1},
                                                    768: {items: 1},
                                                    992: {items: 1},
                                                    1200: {items: 1}
                                                },
                                                dotClass: 'owl2-dot',
                                                dotsClass: 'owl2-dots',
                                                dots: false,
                                                dotsSpeed: 500,
                                                nav: false,
                                                loop: true,
                                                navSpeed: 500,
                                                navText: ['&#171;', '&#187;'],
                                                navClass: ['owl2-prev', 'owl2-next']
                                            });

                                            $featureslider.on('translated.owl.carousel2', function (e) {
                                                var $item_active = $('.product-feature .owl2-item.active', $element);
                                                var $item = $('.product-feature .owl2-item', $element);

                                                _UngetAnimate($item);

                                                if ($item_active.length > 1 && _effect != 'none') {
                                                    _getAnimate($item_active);
                                                } else {
                                                    $item.css({
                                                        'opacity': 1,
                                                        'filter': 'alpha(opacity = 100)'
                                                    });
                                                }
                                            });

                                            function _getAnimate($el) {
                                                if (_effect == 'none') return;
                                                $extraslider.removeClass('extra-animate');
                                                $el.each(function (i) {
                                                    var $_el = $(this);
                                                    $(this).css({
                                                        '-webkit-animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-moz-animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-o-animation': _effect + ' ' + _duration + "ms ease both",
                                                        'animation': _effect + ' ' + _duration + "ms ease both",
                                                        '-webkit-animation-delay': +i * _delay + 'ms',
                                                        '-moz-animation-delay': +i * _delay + 'ms',
                                                        '-o-animation-delay': +i * _delay + 'ms',
                                                        'animation-delay': +i * _delay + 'ms',
                                                        'opacity': 1
                                                    }).animate({
                                                        opacity: 1
                                                    });

                                                    if (i == $el.size() - 1) {
                                                        $extraslider.addClass("extra-animate");
                                                    }
                                                });
                                            }

                                            function _UngetAnimate($el) {
                                                $el.each(function (i) {
                                                    $(this).css({
                                                        'animation': '',
                                                        '-webkit-animation': '',
                                                        '-moz-animation': '',
                                                        '-o-animation': '',
                                                        'opacity': 1
                                                    });
                                                });
                                            }

                                            data = new Date(2013, 10, 26, 12, 00, 00);

                                            function CountDown(date, id) {
                                                dateNow = new Date();
                                                amount = date.getTime() - dateNow.getTime();
                                                if (amount < 0 && $('#' + id).length) {
                                                    $('.' + id).html("Now!");
                                                } else {
                                                    days = 0;
                                                    hours = 0;
                                                    mins = 0;
                                                    secs = 0;
                                                    out = "";
                                                    amount = Math.floor(amount / 1000);
                                                    days = Math.floor(amount / 86400);
                                                    amount = amount % 86400;
                                                    hours = Math.floor(amount / 3600);
                                                    amount = amount % 3600;
                                                    mins = Math.floor(amount / 60);
                                                    amount = amount % 60;
                                                    secs = Math.floor(amount);
                                                    if (days != 0) {
                                                        out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "D :" : "D :") + "</div>" + "</div> ";
                                                    }
                                                    if (days == 0 && hours != 0) {
                                                        out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "H :" : "H :") + "</div>" + "</div> ";
                                                    } else if (hours != 0) {
                                                        out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "H :" : "H :") + "</div>" + "</div> ";
                                                    }
                                                    if (days == 0 && hours != 0) {
                                                        out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    } else if (days == 0 && hours == 0) {
                                                        out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    } else {
                                                        out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "M :" : "M :") + "</div>" + "</div> ";
                                                        out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "S" : "S") + "</div>" + "</div> ";
                                                        out = out.substr(0, out.length - 2);
                                                    }

                                                    $('.' + id).html(out);

                                                    setTimeout(function () {
                                                        CountDown(date, id);
                                                    }, 1000);
                                                }
                                            }

                                            if (listdeal1.length > 0) {
                                                for (var i = 0; i < listdeal1.length; i++) {
                                                    var arr = listdeal1[i].split("|");
                                                    if (arr[1].length) {
                                                        var data = new Date(arr[1]);
                                                        CountDown(data, arr[0]);
                                                    }
                                                }
                                            }
                                        })('#so_deals_12662999911162019080438');
                                    });
                                    //]]>
                                </script>
                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_kjmz block">
                        <div class="banner-21 banner">
                            <div>
                                <a class="bn-shadow" href="#" title="Banner 24">
                                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner21.jpg"
                                         alt="Static Image">
                                </a>
                            </div>

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_b5i1 block">

                        <div class="module so-listing-tabs-ltr home3_listingtab">
                            <div class="head-title">
                                <h3 class="modtitle">Fashion & Accessories</h3>
                            </div>
                            <div class="modcontent">
                                <!--[if lt IE 9]>
                                <div id="so_listing_tabs_244" class="so-listing-tabs msie lt-ie9 first-load module"><![endif]-->
                                <!--[if IE 9]>
                                <div id="so_listing_tabs_244" class="so-listing-tabs msie first-load module"><![endif]-->
                                <!--[if gt IE 9]><!-->
                                <div id="so_listing_tabs_244" class="so-listing-tabs first-load module">
                                    <!--<![endif]-->
                                    <div class="ltabs-wrap products-list grid">
                                        <div class="ltabs-tabs-container" data-delay="500"
                                             data-duration="800"
                                             data-effect="none"
                                             data-ajaxurl="http://opencart.opencartworks.com/themes/so_topdeal3/"
                                             data-type_source="0"
                                             data-type_show="slider">

                                            <div class="ltabs-tabs-wrap">
                                                <span class='ltabs-tab-selected'></span>
                                                <span class="ltabs-tab-arrow">▼</span>
                                                <ul class="ltabs-tabs cf">
                                                    <li class="ltabs-tab   tab-sel tab-loaded "
                                                        data-category-id="32"
                                                        data-active-content-l=".items-category-32"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/116-60x60.jpg"
                                                                 title="Bags" alt="Bags"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Bags
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="36"
                                                        data-active-content-l=".items-category-36"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/23-60x60.jpg"
                                                                 title="Dress Ladies" alt="Dress Ladies"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Dress Ladies
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="31"
                                                        data-active-content-l=".items-category-31"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/24-60x60.jpg"
                                                                 title="Jean" alt="Jean"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Jean
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="27"
                                                        data-active-content-l=".items-category-27"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/9-60x60.jpg"
                                                                 title="Men Fashion" alt="Men Fashion"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Men Fashion
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="88"
                                                        data-active-content-l=".items-category-88"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/22-60x60.jpg"
                                                                 title="T-shirt" alt="T-shirt"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													T-shirt
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="26"
                                                        data-active-content-l=".items-category-26"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/25-60x60.jpg"
                                                                 title="Trending" alt="Trending"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Trending
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="37"
                                                        data-active-content-l=".items-category-37"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/21-60x60.jpg"
                                                                 title="Western Wear" alt="Western Wear"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Western Wear
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="20"
                                                        data-active-content-l=".items-category-20"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/10-60x60.jpg"
                                                                 title="Women Fashion" alt="Women Fashion"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Women Fashion
											</span>
                                                    </li>
                                                </ul>
                                            </div>


                                        </div>
                                        <div class="wap-listing-tabs">
                                            <div class="ltabs-items-container">
                                                <div class="ltabs-items  ltabs-items-selected ltabs-items-loaded items-category-32"
                                                     data-total="6">

                                                    <div class="ltabs-items-inner owl2-carousel  ltabs-slider ">

                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='42'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                               target="_self"
                                                                               title="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/24-270x270.png"
                                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=42"
                                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz"
                                                                                   target="_self">
                                                                                    Est Officia Including
                                                                                    Shoes Beautiful Pieces
                                                                                    Canaz
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('42');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('42');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('42');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='52'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=52"
                                                                               target="_self"
                                                                               title="Invisible Hidden Spy Earphone Micro Wireless">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/16-270x270.png"
                                                                                     alt="Invisible Hidden Spy Earphone Micro Wireless">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=52"
                                                                                   title="Invisible Hidden Spy Earphone Micro Wireless"
                                                                                   target="_self">
                                                                                    Invisible Hidden Spy
                                                                                    Earphone Micro Wireless
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star fa-stack-2x"></i><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star fa-stack-2x"></i><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('52');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('52');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('52');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='87'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=87"
                                                                               target="_self"
                                                                               title="Ligula tortoram ut labore et dolore magna elip">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/14-270x270.png"
                                                                                     alt="Ligula tortoram ut labore et dolore magna elip">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=87"
                                                                                   title="Ligula tortoram ut labore et dolore magna elip"
                                                                                   target="_self">
                                                                                    Ligula tortoram ut
                                                                                    labore et dolore magna
                                                                                    elip
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('87');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('87');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('87');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='90'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=90"
                                                                               target="_self"
                                                                               title="Ligula tortoram ut labore et dolore magna elip">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/11-270x270.png"
                                                                                     alt="Ligula tortoram ut labore et dolore magna elip">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=90"
                                                                                   title="Ligula tortoram ut labore et dolore magna elip"
                                                                                   target="_self">
                                                                                    Ligula tortoram ut
                                                                                    labore et dolore magna
                                                                                    elip
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('90');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('90');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('90');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='83'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=83"
                                                                               target="_self"
                                                                               title="magna elip therefore always free from bolac sodo">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/18-270x270.png"
                                                                                     alt="magna elip therefore always free from bolac sodo">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-2%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=83"
                                                                                   title="magna elip therefore always free from bolac sodo"
                                                                                   target="_self">
                                                                                    magna elip therefore
                                                                                    always free from bolac
                                                                                    sodo
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$119.60</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('83');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('83');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('83');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='29'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                               target="_self"
                                                                               title="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/fashion/15-270x270.png"
                                                                                     alt="Est Officia Including Shoes Beautiful Pieces Canaz">
                                                                            </a>


                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-82%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=29"
                                                                                   title="Est Officia Including Shoes Beautiful Pieces Canaz"
                                                                                   target="_self">
                                                                                    Est Officia Including
                                                                                    Shoes Beautiful Pieces
                                                                                    Canaz
                                                                                </a>
                                                                            </h4>

                                                                            <div class="rating">
                                                                                            <span class="fa fa-stack"><i
                                                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                                <span class="fa fa-stack"><i
                                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                                            </div>


                                                                            <p class="price">
                                                                                <span class="price-new">$62.00</span>
                                                                                <span class="price-old">$337.99</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('29');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('29');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('29');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            var $tag_id = $('#so_listing_tabs_244'),
                                                                parent_active = $('.items-category-32', $tag_id),
                                                                total_product = parent_active.data('total'),
                                                                tab_active = $('.ltabs-items-inner', parent_active),
                                                                nb_column0 = 2,
                                                                nb_column1 = 2,
                                                                nb_column2 = 1,
                                                                nb_column3 = 2,
                                                                nb_column4 = 1;
                                                            tab_active.owlCarousel2({
                                                                rtl: false,
                                                                nav: false,
                                                                dots: false,
                                                                margin: 0,
                                                                loop: false,
                                                                autoplay: false,
                                                                autoplayHoverPause: false,
                                                                autoplayTimeout: 5000,
                                                                autoplaySpeed: 1000,
                                                                mouseDrag: true,
                                                                touchDrag: true,
                                                                navRewind: true,
                                                                navText: ['', ''],
                                                                responsive: {
                                                                    0: {
                                                                        items: nb_column4,
                                                                        nav: total_product <= nb_column4 ? false : true,
                                                                    },
                                                                    480: {
                                                                        items: nb_column3,
                                                                        nav: total_product <= nb_column3 ? false : true,
                                                                    },
                                                                    768: {
                                                                        items: nb_column2,
                                                                        nav: total_product <= nb_column2 ? false : true,
                                                                    },
                                                                    992: {
                                                                        items: nb_column1,
                                                                        nav: total_product <= nb_column1 ? false : true,
                                                                    },
                                                                    1200: {
                                                                        items: nb_column0,
                                                                        nav: total_product <= nb_column0 ? false : true,
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    </script>


                                                </div>
                                                <div class="ltabs-items  items-category-36" data-total="6">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-31" data-total="3">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-27" data-total="10">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-88" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-26" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-37" data-total="4">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-20" data-total="9">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            ;
                                            (function (element) {
                                                var $element = $(element),
                                                    $tab = $('.ltabs-tab', $element),
                                                    $tab_label = $('.ltabs-tab-label', $tab),
                                                    $tabs = $('.ltabs-tabs', $element),
                                                    ajax_url = $tabs.parents('.ltabs-tabs-container').attr('data-ajaxurl'),
                                                    effect = $tabs.parents('.ltabs-tabs-container').attr('data-effect'),
                                                    delay = $tabs.parents('.ltabs-tabs-container').attr('data-delay'),
                                                    duration = $tabs.parents('.ltabs-tabs-container').attr('data-duration'),
                                                    type_source = $tabs.parents('.ltabs-tabs-container').attr('data-type_source'),
                                                    $items_content = $('.ltabs-items', $element),
                                                    $items_inner = $('.ltabs-items-inner', $items_content),
                                                    $items_first_active = $('.ltabs-items-selected', $element),
                                                    $load_more = $('.ltabs-loadmore', $element),
                                                    $btn_loadmore = $('.ltabs-loadmore-btn', $load_more),
                                                    $select_box = $('.ltabs-selectbox', $element),
                                                    $tab_label_select = $('.ltabs-tab-selected', $element),
                                                    setting = 'a:73:{s:6:"action";s:9:"save_edit";s:4:"name";s:34:"Home 3 - Fashion &amp; Accessories";s:18:"module_description";a:2:{i:2;a:1:{s:9:"head_name";s:25:"Fashion &amp; Accessories";}i:1;a:1:{s:9:"head_name";s:25:"Fashion &amp; Accessories";}}s:9:"head_name";s:25:"Fashion &amp; Accessories";s:17:"disp_title_module";s:1:"1";s:6:"status";s:1:"1";s:12:"store_layout";s:6:"style3";s:12:"class_suffix";s:16:"home3_listingtab";s:16:"item_link_target";s:5:"_self";s:10:"nb_column0";s:1:"2";s:10:"nb_column1";s:1:"2";s:10:"nb_column2";s:1:"1";s:10:"nb_column3";s:1:"2";s:10:"nb_column4";s:1:"1";s:9:"type_show";s:6:"slider";s:6:"nb_row";s:1:"2";s:11:"type_source";s:1:"0";s:8:"category";a:8:{i:0;s:2:"32";i:1;s:2:"20";i:2;s:2:"88";i:3;s:2:"26";i:4;s:2:"27";i:5;s:2:"31";i:6;s:2:"36";i:7;s:2:"37";}s:14:"child_category";s:1:"1";s:14:"category_depth";s:1:"1";s:12:"product_sort";s:7:"p.price";s:16:"product_ordering";s:3:"ASC";s:12:"source_limit";s:1:"6";s:13:"catid_preload";s:2:"32";s:17:"field_product_tab";s:0:"";s:15:"tab_all_display";s:1:"0";s:18:"tab_max_characters";s:2:"25";s:16:"tab_icon_display";s:1:"1";s:12:"cat_order_by";s:4:"name";s:15:"imgcfgcat_width";s:2:"60";s:16:"imgcfgcat_height";s:2:"60";s:13:"display_title";s:1:"1";s:15:"title_maxlength";s:2:"50";s:19:"display_description";s:1:"0";s:21:"description_maxlength";s:3:"100";s:13:"display_price";s:1:"1";s:19:"display_add_to_cart";s:1:"1";s:16:"display_wishlist";s:1:"1";s:15:"display_compare";s:1:"1";s:14:"display_rating";s:1:"1";s:12:"display_sale";s:1:"1";s:11:"display_new";s:1:"1";s:8:"date_day";s:3:"227";s:17:"product_image_num";s:1:"1";s:13:"product_image";s:1:"1";s:22:"product_get_image_data";s:1:"1";s:23:"product_get_image_image";s:1:"1";s:5:"width";s:3:"270";s:6:"height";s:3:"270";s:24:"product_placeholder_path";s:11:"http://opencart.opencartworks.com/themes/so_topdeal3/nophoto.png";s:20:"display_banner_image";s:1:"0";s:12:"banner_image";s:12:"http://opencart.opencartworks.com/themes/so_topdeal3/no_image.png";s:12:"banner_width";s:3:"150";s:13:"banner_height";s:3:"250";s:16:"banner_image_url";s:0:"";s:6:"effect";s:4:"none";s:8:"duration";s:3:"800";s:5:"delay";s:3:"500";s:8:"autoplay";s:1:"0";s:11:"display_nav";s:1:"1";s:12:"display_dots";s:1:"0";s:12:"display_loop";s:1:"0";s:9:"touchdrag";s:1:"1";s:9:"mousedrag";s:1:"1";s:10:"pausehover";s:1:"0";s:15:"autoplayTimeout";s:4:"5000";s:13:"autoplaySpeed";s:4:"1000";s:8:"pre_text";s:0:"";s:9:"post_text";s:0:"";s:9:"use_cache";s:1:"0";s:10:"cache_time";s:4:"3600";s:8:"moduleid";s:3:"244";s:5:"start";i:0;}',
                                                    type_show = 'slider';

                                                enableSelectBoxes();

                                                function enableSelectBoxes() {
                                                    $tab_wrap = $('.ltabs-tabs-wrap', $element),
                                                        $tab_label_select.html($('.ltabs-tab', $element).filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    if ($(window).innerWidth() <= 479) {
                                                        $tab_wrap.addClass('ltabs-selectbox');
                                                    } else {
                                                        $tab_wrap.removeClass('ltabs-selectbox');
                                                    }
                                                }

                                                $('span.ltabs-tab-selected, span.ltabs-tab-arrow', $element).click(function () {
                                                    if ($('.ltabs-tabs', $element).hasClass('ltabs-open')) {
                                                        $('.ltabs-tabs', $element).removeClass('ltabs-open');
                                                    } else {
                                                        $('.ltabs-tabs', $element).addClass('ltabs-open');
                                                    }
                                                });

                                                $(window).resize(function () {
                                                    if ($(window).innerWidth() <= 479) {
                                                        $('.ltabs-tabs-wrap', $element).addClass('ltabs-selectbox');
                                                    } else {
                                                        $('.ltabs-tabs-wrap', $element).removeClass('ltabs-selectbox');
                                                    }
                                                });

                                                function showAnimateItems(el) {
                                                    var $_items = $('.new-ltabs-item', el), nub = 0;
                                                    $('.ltabs-loadmore-btn', el).fadeOut('fast');
                                                    $_items.each(function (i) {
                                                        nub++;
                                                        switch (effect) {
                                                            case 'none' :
                                                                $(this).css({
                                                                    'opacity': '1',
                                                                    'filter': 'alpha(opacity = 100)'
                                                                });
                                                                break;
                                                            default:
                                                                animatesItems($(this), nub * delay, i, el);
                                                        }
                                                        if (i == $_items.length - 1) {
                                                            $('.ltabs-loadmore-btn', el).fadeIn(3000);
                                                        }
                                                        $(this).removeClass('new-ltabs-item');
                                                    });
                                                }

                                                function animatesItems($this, fdelay, i, el) {
                                                    var $_items = $('.ltabs-item', el);
                                                    $this.stop(true, true).attr("style",
                                                        "-webkit-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation:" + effect + " " + duration + "ms;"
                                                        + "-o-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation-delay:" + fdelay + "ms;"
                                                        + "-webkit-animation-delay:" + fdelay + "ms;"
                                                        + "-o-animation-delay:" + fdelay + "ms;"
                                                        + "animation-delay:" + fdelay + "ms;").delay(fdelay).animate({
                                                        opacity: 1,
                                                        filter: 'alpha(opacity = 100)'
                                                    }, {
                                                        delay: 1000
                                                    });
                                                    if (i == ($_items.length - 1)) {
                                                        $(".ltabs-items-inner").addClass("play");
                                                    }
                                                }

                                                if (type_show == 'loadmore') {
                                                    showAnimateItems($items_first_active);
                                                }
                                                $tab.on('click.ltabs-tab', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('tab-sel')) return false;
                                                    if ($this.parents('.ltabs-tabs').hasClass('ltabs-open')) {
                                                        $this.parents('.ltabs-tabs').removeClass('ltabs-open');
                                                    }
                                                    $tab.removeClass('tab-sel');
                                                    $this.addClass('tab-sel');
                                                    var items_active = $this.attr('data-active-content-l');
                                                    var _items_active = $(items_active, $element);
                                                    $items_content.removeClass('ltabs-items-selected');
                                                    _items_active.addClass('ltabs-items-selected');
                                                    $tab_label_select.html($tab.filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    var $loading = $('.ltabs-loading', _items_active);
                                                    var loaded = _items_active.hasClass('ltabs-items-loaded');
                                                    type_show = $tabs.parents('.ltabs-tabs-container').attr('data-type_show');
                                                    if (!loaded && !_items_active.hasClass('ltabs-process')) {
                                                        _items_active.addClass('ltabs-process');
                                                        var category_id = $this.attr('data-category-id');
                                                        $loading.show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: ajax_url,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: 0,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 244
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $('.ltabs-loading', _items_active).replaceWith(data.items_markup);
                                                                    _items_active.addClass('ltabs-items-loaded').removeClass('ltabs-process');
                                                                    $loading.remove();
                                                                    if (type_show != 'slider') {
                                                                        showAnimateItems(_items_active);
                                                                    }
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }

                                                            },
                                                            dataType: 'json'
                                                        });

                                                    } else {
                                                        if (type_show == 'loadmore') {
                                                            $('.ltabs-item', $items_content).removeAttr('style').addClass('new-ltabs-item');
                                                            showAnimateItems(_items_active);
                                                        } else {
                                                            var owl = $('.owl2-carousel', _items_active);
                                                            owl = owl.data('owlCarousel2');
                                                            if (typeof owl !== 'undefined') {
                                                                owl.onResize();
                                                            }
                                                        }
                                                    }
                                                });

                                                function updateStatus($el) {
                                                    $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    var countitem = $('.ltabs-item', $el).length;
                                                    $('.ltabs-image-loading', $el).css({display: 'none'});
                                                    $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_start', countitem);
                                                    var rl_total = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_total');
                                                    var rl_load = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_load');
                                                    var rl_allready = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_allready');

                                                    if (countitem >= rl_total) {
                                                        $('.ltabs-loadmore-btn', $el).addClass('loaded');
                                                        $('.ltabs-image-loading', $el).css({display: 'none'});
                                                        $('.ltabs-loadmore-btn', $el).attr('data-label', rl_allready);
                                                        $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    }
                                                }

                                                $btn_loadmore.on('click.loadmore', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('loaded') || $this.hasClass('loading')) {
                                                        return false;
                                                    } else {
                                                        $this.addClass('loading');
                                                        $('.ltabs-image-loading', $this).css({display: 'inline-block'});
                                                        var rl_start = $this.parent().attr('data-rl_start'),
                                                            rl_ajaxurl = $this.parent().attr('data-ajaxurl'),
                                                            effect = $this.parent().attr('data-effect'),
                                                            category_id = $this.parent().attr('data-categoryid'),
                                                            items_active = $this.parent().attr('data-active-content');

                                                        var _items_active = $(items_active, $element);

                                                        $.ajax({
                                                            type: 'POST',
                                                            url: rl_ajaxurl,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: rl_start,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 244
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $($(data.items_markup).html()).insertAfter($('.ltabs-item', _items_active).nextAll().last());
                                                                    $('.ltabs-image-loading', $this).css({display: 'none'});
                                                                    showAnimateItems(_items_active);
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }
                                                            }, dataType: 'json'
                                                        });
                                                    }
                                                    return false;
                                                });
                                            })('#so_listing_tabs_244');
                                        });
                                        //]]>
                                    </script>
                                </div>
                            </div> <!-- /.modcontent-->

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_gmfs block">

                        <div class="module so-listing-tabs-ltr home3_listingtab_style2">
                            <div class="head-title">
                                <h3 class="modtitle"><span>Digital & Electronic</span></h3>
                            </div>
                            <div class="modcontent">
                                <!--[if lt IE 9]>
                                <div id="so_listing_tabs_246" class="so-listing-tabs msie lt-ie9 first-load module"><![endif]-->
                                <!--[if IE 9]>
                                <div id="so_listing_tabs_246" class="so-listing-tabs msie first-load module"><![endif]-->
                                <!--[if gt IE 9]><!-->
                                <div id="so_listing_tabs_246" class="so-listing-tabs first-load module">
                                    <!--<![endif]-->
                                    <div class="ltabs-wrap">
                                        <div class="ltabs-tabs-container" data-delay="500"
                                             data-duration="800"
                                             data-effect="none"
                                             data-ajaxurl="http://opencart.opencartworks.com/themes/so_topdeal3/"
                                             data-type_source="0"
                                             data-type_show="slider">

                                            <div class="ltabs-tabs-wrap">
                                                <span class='ltabs-tab-selected'></span>
                                                <span class="ltabs-tab-arrow">▼</span>
                                                <ul class="ltabs-tabs cf">
                                                    <li class="ltabs-tab   tab-sel tab-loaded "
                                                        data-category-id="28"
                                                        data-active-content-l=".items-category-28"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/94-60x60.jpg"
                                                                 title="Case" alt="Case"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Case
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="30"
                                                        data-active-content-l=".items-category-30"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/93-60x60.jpg"
                                                                 title="Cell &amp; Cable"
                                                                 alt="Cell &amp; Cable"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Cell &amp; Cable
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="89"
                                                        data-active-content-l=".items-category-89"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/88-60x60.jpg"
                                                                 title="Headphone" alt="Headphone"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Headphone
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="90"
                                                        data-active-content-l=".items-category-90"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/90-60x60.jpg"
                                                                 title="Laptops" alt="Laptops"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Laptops
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="77"
                                                        data-active-content-l=".items-category-77"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/87-60x60.jpg"
                                                                 title="Mobile &amp; Table"
                                                                 alt="Mobile &amp; Table"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Mobile &amp; Table
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="79"
                                                        data-active-content-l=".items-category-79"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/91-60x60.jpg"
                                                                 title="Sound" alt="Sound"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Sound
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="80"
                                                        data-active-content-l=".items-category-80"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/89-60x60.jpg"
                                                                 title="USB &amp; HDD" alt="USB &amp; HDD"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													USB &amp; HDD
											</span>
                                                    </li>
                                                    <li class="ltabs-tab  "
                                                        data-category-id="62"
                                                        data-active-content-l=".items-category-62"
                                                    >
                                                        <div class="ltabs-tab-img">
                                                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/category/92-60x60.jpg"
                                                                 title="Video &amp; Camera"
                                                                 alt="Video &amp; Camera"
                                                                 style="width: 60px; height:60px;background:#fff"/>
                                                        </div>
                                                        <span class="ltabs-tab-label">
													Video &amp; Camera
											</span>
                                                    </li>
                                                </ul>
                                            </div>


                                        </div>
                                        <div class="wap-listing-tabs products-list grid">

                                            <div class="ltabs-items-container">
                                                <div class="ltabs-items  ltabs-items-selected ltabs-items-loaded items-category-28"
                                                     data-total="15">

                                                    <div class="ltabs-items-inner owl2-carousel  ltabs-slider ">

                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='103'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery)">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery)">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                                                   title="Compact Portable Charger (External Battery)"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $98.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('103');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('103');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('103');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='51'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                               target="_self"
                                                                               title="Charger  Compact Portable with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                                                     alt="Charger  Compact Portable with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                                                   title="Charger  Compact Portable with Premium"
                                                                                   target="_self">
                                                                                    Charger Compact Portable
                                                                                    with Premium
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('51');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('51');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('51');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='111'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                                               target="_self"
                                                                               title="Compact (External Battery Power Bank) with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/12-270x270.jpg"
                                                                                     alt="Compact (External Battery Power Bank) with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                                                   title="Compact (External Battery Power Bank) with Premium"
                                                                                   target="_self">
                                                                                    Compact (External
                                                                                    Battery Power Bank) wi..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('111');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('111');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('111');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='75'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery) T21">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/14-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery) T21">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                                                   title="Compact Portable Charger (External Battery) T21"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('75');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('75');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('75');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='88'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (External Battery) T22">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/21-270x270.jpg"
                                                                                     alt="Compact Portable Charger (External Battery) T22">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                                                   title="Compact Portable Charger (External Battery) T22"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (External Batte..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('88');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('88');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('88');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='58'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                               target="_self"
                                                                               title="Computer Science saepe eveniet ut et volu redae">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                                                     alt="Computer Science saepe eveniet ut et volu redae">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-2%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                                                   title="Computer Science saepe eveniet ut et volu redae"
                                                                                   target="_self">
                                                                                    Computer Science saepe
                                                                                    eveniet ut et vol..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$119.60</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('58');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('58');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('58');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='105'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                               target="_self"
                                                                               title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                                                     alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                                                   title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium"
                                                                                   target="_self">
                                                                                    Lorem Ipsum dolor at
                                                                                    vero eos et iusto o..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('105');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('105');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('105');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='110'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=110"
                                                                               target="_self"
                                                                               title="Mammo Diablo except to obtain some advan from">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/6-270x270.jpg"
                                                                                     alt="Mammo Diablo except to obtain some advan from">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=110"
                                                                                   title="Mammo Diablo except to obtain some advan from"
                                                                                   target="_self">
                                                                                    Mammo Diablo except to
                                                                                    obtain some advan..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $122.00

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('110');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('110');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('110');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='78'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                               target="_self"
                                                                               title="Portable  Compact Charger (External Battery) t45">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                                                     alt="Portable  Compact Charger (External Battery) t45">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-20%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                                                   title="Portable  Compact Charger (External Battery) t45"
                                                                                   target="_self">
                                                                                    Portable Compact Charger
                                                                                    (External Batt..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$98.00</span>
                                                                                <span class="price-old">$122.00</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('78');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('78');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('78');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='40'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=40"
                                                                               target="_self"
                                                                               title="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/28-270x270.jpg"
                                                                                     alt="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=40"
                                                                                   title="LG 29UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 HDMI(2)"
                                                                                   target="_self">
                                                                                    LG 29UC97-S 29"(21:9)
                                                                                    FHD IPS LED 2560X..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $123.20

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('40');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('40');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('40');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="ltabs-item ">

                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='66'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                               target="_self"
                                                                               title="Compact Portable Charger (Power Bank) with Premium">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                                                     alt="Compact Portable Charger (Power Bank) with Premium">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                            <span class="label-product label-sale">-70%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                                                   title="Compact Portable Charger (Power Bank) with Premium"
                                                                                   target="_self">
                                                                                    Compact Portable Charger
                                                                                    (Power Bank) wi..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                <span class="price-new">$74.00</span>
                                                                                <span class="price-old">$241.99</span>

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('66');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('66');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('66');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item-inner product-thumb trg transition product-layout">
                                                                <div class="product-item-container">
                                                                    <div class="left-block ">
                                                                        <div class="image product-image-container so-quickview ">

                                                                            <a class="lt-image"
                                                                               data-product='64'
                                                                               href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=64"
                                                                               target="_self"
                                                                               title="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 ">
                                                                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/5-270x270.jpg"
                                                                                     alt="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 ">
                                                                            </a>
                                                                        </div>
                                                                        <div class="box-label">
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">

                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=64"
                                                                                   title="SamSung 23UC97-S 29&quot;(21:9) FHD  IPS LED 2560X1080 "
                                                                                   target="_self">
                                                                                    SamSung 23UC97-S
                                                                                    29"(21:9) FHD IPS LED ..
                                                                                </a>
                                                                            </h4>
                                                                            <p class="price">
                                                                                $337.99

                                                                            </p>
                                                                        </div>

                                                                        <div class="button-group2">
                                                                            <button class="bt-cart addToCart"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to cart"
                                                                                    onclick="cart.add('64');">
                                                                                <span>Add to cart</span>
                                                                            </button>
                                                                            <button class="bt wishlist"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Add to Wish List"
                                                                                    onclick="wishlist.add('64');">
                                                                                <i class="fa fa-heart"></i>
                                                                            </button>

                                                                            <button class="bt compare"
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    title="Compare this Product"
                                                                                    onclick="compare.add('64');">
                                                                                <i class="fa fa-exchange"></i>
                                                                            </button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            var $tag_id = $('#so_listing_tabs_246'),
                                                                parent_active = $('.items-category-28', $tag_id),
                                                                total_product = parent_active.data('total'),
                                                                tab_active = $('.ltabs-items-inner', parent_active),
                                                                nb_column0 = 4,
                                                                nb_column1 = 3,
                                                                nb_column2 = 2,
                                                                nb_column3 = 2,
                                                                nb_column4 = 1;
                                                            tab_active.owlCarousel2({
                                                                rtl: false,
                                                                nav: true,
                                                                dots: false,
                                                                margin: 0,
                                                                loop: false,
                                                                autoplay: false,
                                                                autoplayHoverPause: false,
                                                                autoplayTimeout: 5000,
                                                                autoplaySpeed: 1000,
                                                                mouseDrag: true,
                                                                touchDrag: true,
                                                                navRewind: true,
                                                                navText: ['', ''],
                                                                responsive: {
                                                                    0: {
                                                                        items: nb_column4,
                                                                        nav: total_product <= nb_column4 ? false : true,
                                                                    },
                                                                    480: {
                                                                        items: nb_column3,
                                                                        nav: total_product <= nb_column3 ? false : true,
                                                                    },
                                                                    768: {
                                                                        items: nb_column2,
                                                                        nav: total_product <= nb_column2 ? false : true,
                                                                    },
                                                                    992: {
                                                                        items: nb_column1,
                                                                        nav: total_product <= nb_column1 ? false : true,
                                                                    },
                                                                    1200: {
                                                                        items: nb_column0,
                                                                        nav: total_product <= nb_column0 ? false : true,
                                                                    },
                                                                    1400: {
                                                                        items: 5,

                                                                    }
                                                                }
                                                            });
                                                        });
                                                    </script>


                                                </div>
                                                <div class="ltabs-items  items-category-30" data-total="16">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-89" data-total="14">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-90" data-total="12">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-77" data-total="5">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-79" data-total="13">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-80" data-total="14">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                                <div class="ltabs-items  items-category-62" data-total="16">
                                                    <div class="ltabs-loading"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        jQuery(document).ready(function ($) {
                                            ;
                                            (function (element) {
                                                var $element = $(element),
                                                    $tab = $('.ltabs-tab', $element),
                                                    $tab_label = $('.ltabs-tab-label', $tab),
                                                    $tabs = $('.ltabs-tabs', $element),
                                                    ajax_url = $tabs.parents('.ltabs-tabs-container').attr('data-ajaxurl'),
                                                    effect = $tabs.parents('.ltabs-tabs-container').attr('data-effect'),
                                                    delay = $tabs.parents('.ltabs-tabs-container').attr('data-delay'),
                                                    duration = $tabs.parents('.ltabs-tabs-container').attr('data-duration'),
                                                    type_source = $tabs.parents('.ltabs-tabs-container').attr('data-type_source'),
                                                    $items_content = $('.ltabs-items', $element),
                                                    $items_inner = $('.ltabs-items-inner', $items_content),
                                                    $items_first_active = $('.ltabs-items-selected', $element),
                                                    $load_more = $('.ltabs-loadmore', $element),
                                                    $btn_loadmore = $('.ltabs-loadmore-btn', $load_more),
                                                    $select_box = $('.ltabs-selectbox', $element),
                                                    $tab_label_select = $('.ltabs-tab-selected', $element),
                                                    setting = 'a:73:{s:6:"action";s:9:"save_edit";s:4:"name";s:32:"Home3 - Digital &amp; Electronic";s:18:"module_description";a:2:{i:2;a:1:{s:9:"head_name";s:24:"Digital &amp; Electronic";}i:1;a:1:{s:9:"head_name";s:24:"Digital &amp; Electronic";}}s:9:"head_name";s:24:"Digital &amp; Electronic";s:17:"disp_title_module";s:1:"1";s:6:"status";s:1:"1";s:12:"store_layout";s:6:"style5";s:12:"class_suffix";s:23:"home3_listingtab_style2";s:16:"item_link_target";s:5:"_self";s:10:"nb_column0";s:1:"4";s:10:"nb_column1";s:1:"3";s:10:"nb_column2";s:1:"2";s:10:"nb_column3";s:1:"2";s:10:"nb_column4";s:1:"1";s:9:"type_show";s:6:"slider";s:6:"nb_row";s:1:"2";s:11:"type_source";s:1:"0";s:8:"category";a:8:{i:0;s:2:"28";i:1;s:2:"89";i:2;s:2:"90";i:3;s:2:"77";i:4;s:2:"79";i:5;s:2:"80";i:6;s:2:"30";i:7;s:2:"62";}s:14:"child_category";s:1:"1";s:14:"category_depth";s:1:"1";s:12:"product_sort";s:7:"p.price";s:16:"product_ordering";s:3:"ASC";s:12:"source_limit";s:2:"12";s:13:"catid_preload";s:2:"28";s:17:"field_product_tab";s:0:"";s:15:"tab_all_display";s:1:"0";s:18:"tab_max_characters";s:2:"50";s:16:"tab_icon_display";s:1:"1";s:12:"cat_order_by";s:4:"name";s:15:"imgcfgcat_width";s:2:"60";s:16:"imgcfgcat_height";s:2:"60";s:13:"display_title";s:1:"1";s:15:"title_maxlength";s:2:"40";s:19:"display_description";s:1:"0";s:21:"description_maxlength";s:3:"100";s:13:"display_price";s:1:"1";s:19:"display_add_to_cart";s:1:"1";s:16:"display_wishlist";s:1:"1";s:15:"display_compare";s:1:"1";s:14:"display_rating";s:1:"0";s:12:"display_sale";s:1:"1";s:11:"display_new";s:1:"1";s:8:"date_day";s:3:"227";s:17:"product_image_num";s:1:"1";s:13:"product_image";s:1:"1";s:22:"product_get_image_data";s:1:"1";s:23:"product_get_image_image";s:1:"1";s:5:"width";s:3:"270";s:6:"height";s:3:"270";s:24:"product_placeholder_path";s:11:"http://opencart.opencartworks.com/themes/so_topdeal3/nophoto.png";s:20:"display_banner_image";s:1:"0";s:12:"banner_image";s:12:"http://opencart.opencartworks.com/themes/so_topdeal3/no_image.png";s:12:"banner_width";s:3:"150";s:13:"banner_height";s:3:"250";s:16:"banner_image_url";s:0:"";s:6:"effect";s:4:"none";s:8:"duration";s:3:"800";s:5:"delay";s:3:"500";s:8:"autoplay";s:1:"0";s:11:"display_nav";s:1:"1";s:12:"display_dots";s:1:"0";s:12:"display_loop";s:1:"0";s:9:"touchdrag";s:1:"1";s:9:"mousedrag";s:1:"1";s:10:"pausehover";s:1:"0";s:15:"autoplayTimeout";s:4:"5000";s:13:"autoplaySpeed";s:4:"1000";s:8:"pre_text";s:0:"";s:9:"post_text";s:0:"";s:9:"use_cache";s:1:"0";s:10:"cache_time";s:4:"3600";s:8:"moduleid";s:3:"246";s:5:"start";i:0;}',
                                                    type_show = 'slider';

                                                enableSelectBoxes();

                                                function enableSelectBoxes() {
                                                    $tab_wrap = $('.ltabs-tabs-wrap', $element),
                                                        $tab_label_select.html($('.ltabs-tab', $element).filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    if ($(window).innerWidth() <= 479) {
                                                        $tab_wrap.addClass('ltabs-selectbox');
                                                    } else {
                                                        $tab_wrap.removeClass('ltabs-selectbox');
                                                    }
                                                }

                                                $('span.ltabs-tab-selected, span.ltabs-tab-arrow', $element).click(function () {
                                                    if ($('.ltabs-tabs', $element).hasClass('ltabs-open')) {
                                                        $('.ltabs-tabs', $element).removeClass('ltabs-open');
                                                    } else {
                                                        $('.ltabs-tabs', $element).addClass('ltabs-open');
                                                    }
                                                });

                                                $(window).resize(function () {
                                                    if ($(window).innerWidth() <= 479) {
                                                        $('.ltabs-tabs-wrap', $element).addClass('ltabs-selectbox');
                                                    } else {
                                                        $('.ltabs-tabs-wrap', $element).removeClass('ltabs-selectbox');
                                                    }
                                                });

                                                function showAnimateItems(el) {
                                                    var $_items = $('.new-ltabs-item', el), nub = 0;
                                                    $('.ltabs-loadmore-btn', el).fadeOut('fast');
                                                    $_items.each(function (i) {
                                                        nub++;
                                                        switch (effect) {
                                                            case 'none' :
                                                                $(this).css({
                                                                    'opacity': '1',
                                                                    'filter': 'alpha(opacity = 100)'
                                                                });
                                                                break;
                                                            default:
                                                                animatesItems($(this), nub * delay, i, el);
                                                        }
                                                        if (i == $_items.length - 1) {
                                                            $('.ltabs-loadmore-btn', el).fadeIn(3000);
                                                        }
                                                        $(this).removeClass('new-ltabs-item');
                                                    });
                                                }

                                                function animatesItems($this, fdelay, i, el) {
                                                    var $_items = $('.ltabs-item', el);
                                                    $this.stop(true, true).attr("style",
                                                        "-webkit-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation:" + effect + " " + duration + "ms;"
                                                        + "-o-animation:" + effect + " " + duration + "ms;"
                                                        + "-moz-animation-delay:" + fdelay + "ms;"
                                                        + "-webkit-animation-delay:" + fdelay + "ms;"
                                                        + "-o-animation-delay:" + fdelay + "ms;"
                                                        + "animation-delay:" + fdelay + "ms;").delay(fdelay).animate({
                                                        opacity: 1,
                                                        filter: 'alpha(opacity = 100)'
                                                    }, {
                                                        delay: 1000
                                                    });
                                                    if (i == ($_items.length - 1)) {
                                                        $(".ltabs-items-inner").addClass("play");
                                                    }
                                                }

                                                if (type_show == 'loadmore') {
                                                    showAnimateItems($items_first_active);
                                                }
                                                $tab.on('click.ltabs-tab', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('tab-sel')) return false;
                                                    if ($this.parents('.ltabs-tabs').hasClass('ltabs-open')) {
                                                        $this.parents('.ltabs-tabs').removeClass('ltabs-open');
                                                    }
                                                    $tab.removeClass('tab-sel');
                                                    $this.addClass('tab-sel');
                                                    var items_active = $this.attr('data-active-content-l');
                                                    var _items_active = $(items_active, $element);
                                                    $items_content.removeClass('ltabs-items-selected');
                                                    _items_active.addClass('ltabs-items-selected');
                                                    $tab_label_select.html($tab.filter('.tab-sel').children('.ltabs-tab-label').html());
                                                    var $loading = $('.ltabs-loading', _items_active);
                                                    var loaded = _items_active.hasClass('ltabs-items-loaded');
                                                    type_show = $tabs.parents('.ltabs-tabs-container').attr('data-type_show');
                                                    if (!loaded && !_items_active.hasClass('ltabs-process')) {
                                                        _items_active.addClass('ltabs-process');
                                                        var category_id = $this.attr('data-category-id');
                                                        $loading.show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: ajax_url,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: 0,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 246
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $('.ltabs-loading', _items_active).replaceWith(data.items_markup);
                                                                    _items_active.addClass('ltabs-items-loaded').removeClass('ltabs-process');
                                                                    $loading.remove();
                                                                    if (type_show != 'slider') {
                                                                        showAnimateItems(_items_active);
                                                                    }
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }

                                                            },
                                                            dataType: 'json'
                                                        });

                                                    } else {
                                                        if (type_show == 'loadmore') {
                                                            $('.ltabs-item', $items_content).removeAttr('style').addClass('new-ltabs-item');
                                                            showAnimateItems(_items_active);
                                                        } else {
                                                            var owl = $('.owl2-carousel', _items_active);
                                                            owl = owl.data('owlCarousel2');
                                                            if (typeof owl !== 'undefined') {
                                                                owl.onResize();
                                                            }
                                                        }
                                                    }
                                                });

                                                function updateStatus($el) {
                                                    $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    var countitem = $('.ltabs-item', $el).length;
                                                    $('.ltabs-image-loading', $el).css({display: 'none'});
                                                    $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_start', countitem);
                                                    var rl_total = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_total');
                                                    var rl_load = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_load');
                                                    var rl_allready = $('.ltabs-loadmore-btn', $el).parent().attr('data-rl_allready');

                                                    if (countitem >= rl_total) {
                                                        $('.ltabs-loadmore-btn', $el).addClass('loaded');
                                                        $('.ltabs-image-loading', $el).css({display: 'none'});
                                                        $('.ltabs-loadmore-btn', $el).attr('data-label', rl_allready);
                                                        $('.ltabs-loadmore-btn', $el).removeClass('loading');
                                                    }
                                                }

                                                $btn_loadmore.on('click.loadmore', function () {
                                                    var $this = $(this);
                                                    if ($this.hasClass('loaded') || $this.hasClass('loading')) {
                                                        return false;
                                                    } else {
                                                        $this.addClass('loading');
                                                        $('.ltabs-image-loading', $this).css({display: 'inline-block'});
                                                        var rl_start = $this.parent().attr('data-rl_start'),
                                                            rl_ajaxurl = $this.parent().attr('data-ajaxurl'),
                                                            effect = $this.parent().attr('data-effect'),
                                                            category_id = $this.parent().attr('data-categoryid'),
                                                            items_active = $this.parent().attr('data-active-content');

                                                        var _items_active = $(items_active, $element);

                                                        $.ajax({
                                                            type: 'POST',
                                                            url: rl_ajaxurl,
                                                            data: {
                                                                is_ajax_listing_tabs: 1,
                                                                ajax_reslisting_start: rl_start,
                                                                categoryid: category_id,
                                                                setting: setting,
                                                                lbmoduleid: 246
                                                            },
                                                            success: function (data) {
                                                                if (data.items_markup != '') {
                                                                    $($(data.items_markup).html()).insertAfter($('.ltabs-item', _items_active).nextAll().last());
                                                                    $('.ltabs-image-loading', $this).css({display: 'none'});
                                                                    showAnimateItems(_items_active);
                                                                    updateStatus(_items_active);
                                                                }
                                                                if (typeof (_SoQuickView) != 'undefined') {
                                                                    _SoQuickView();
                                                                }
                                                            }, dataType: 'json'
                                                        });
                                                    }
                                                    return false;
                                                });
                                            })('#so_listing_tabs_246');
                                        });
                                        //]]>
                                    </script>
                                </div>
                            </div> <!-- /.modcontent-->

                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_2xqd block">
                        <div class="banner-layout-5 row clearfix">
                            <div class="banner-22 col-sm-4  banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 22">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner22.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>
                            <div class="banner-23 col-sm-4 banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 23">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner23.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>

                            <div class="banner-24 col-sm-4  banners">
                                <div>
                                    <a class="bn-shadow" href="#" title="Banner 24">
                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner24.jpg"
                                             alt="Static Image">
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_u7mo  block">

                <div class="module so-extraslider-ltr home3_extra_style3">

                    <h3 class="modtitle">Feature Items</h3>


                    <div class="modcontent">


                        <div id="so_extra_slider_20522393041573891479"
                             class="so-extraslider buttom-type1 preset00-6 preset01-5 preset02-3 preset03-2 preset04-1 button-type1">
                            <div class="box-banner">
                                <div class="banners">
                                </div>
                            </div>
                            <!-- Begin extraslider-inner -->
                            <div class="extraslider-inner products-list grid" data-effect="none">

                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='104'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                       target="_self"
                                                       title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/25-270x270.jpg"
                                                             alt="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2)">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-82% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=104"
                                                           target="_self"
                                                           title="Toshiba Pro 21&quot;(21:9) FHD  IPS LED 1920X1080 HDMI(2) ">
                                                            Toshiba Pro 21&quot;(21:9) FHD IPS LED 1920X1080
                                                            HDMI(2)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$62.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$337.99 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('104');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('104');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('104');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='66'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                       target="_self"
                                                       title="Compact Portable Charger (Power Bank) with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/19-270x270.jpg"
                                                             alt="Compact Portable Charger (Power Bank) with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-70% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=66"
                                                           target="_self"
                                                           title="Compact Portable Charger (Power Bank) with Premium ">
                                                            Compact Portable Charger (Power Bank) with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$241.99 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('66');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('66');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('66');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='105'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                       target="_self"
                                                       title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/26-270x270.jpg"
                                                             alt="Lorem Ipsum dolor at vero eos et iusto odi  with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-40% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=105"
                                                           target="_self"
                                                           title="Lorem Ipsum dolor at vero eos et iusto odi  with Premium ">
                                                            Lorem Ipsum dolor at vero eos et iusto odi with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$74.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('105');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('105');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('105');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='30'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                       target="_self"
                                                       title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/1-270x270.jpg"
                                                             alt=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=30"
                                                           target="_self"
                                                           title=" Magnetic Air Vent Phone Holder for iPhone 7 / 7 Plus ">
                                                            Magnetic Air Vent Phone Holder for iPhone 7 / 7
                                                            Plus
                                                        </a>
                                                    </h4>


                                                    <div class="rating">

                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star fa-stack-2x"></i><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('30');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('30');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('30');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='51'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                       target="_self"
                                                       title="Charger  Compact Portable with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/11-270x270.jpg"
                                                             alt="Charger  Compact Portable with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=51"
                                                           target="_self"
                                                           title="Charger  Compact Portable with Premium ">
                                                            Charger Compact Portable with Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('51');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('51');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('51');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='103'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/13-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery)">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=103"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) ">
                                                            Compact Portable Charger (External Battery)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$98.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('103');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('103');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('103');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='88'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery) T22">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/21-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery) T22">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=88"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) T22 ">
                                                            Compact Portable Charger (External Battery) T22
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('88');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('88');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('88');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='78'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                       target="_self"
                                                       title="Portable  Compact Charger (External Battery) t45">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/4-270x270.jpg"
                                                             alt="Portable  Compact Charger (External Battery) t45">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-20% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=78"
                                                           target="_self"
                                                           title="Portable  Compact Charger (External Battery) t45 ">
                                                            Portable Compact Charger (External Battery) t45
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$98.00 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('78');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('78');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('78');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='86'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                       target="_self"
                                                       title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/29-270x270.jpg"
                                                             alt="Seneo PA046 Fast Charger  Wireless Sleep-Friendly)">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-11% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=86"
                                                           target="_self"
                                                           title="Seneo PA046 Fast Charger  Wireless Sleep-Friendly) ">
                                                            Seneo PA046 Fast Charger Wireless
                                                            Sleep-Friendly)
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$108.80 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('86');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('86');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('86');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='58'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                       target="_self"
                                                       title="Computer Science saepe eveniet ut et volu redae">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/23-270x270.jpg"
                                                             alt="Computer Science saepe eveniet ut et volu redae">
                                                    </a>

                                                </div>
                                                <div class="box-label">

                                                    <span class="label-product label-sale">-2% </span>


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=58"
                                                           target="_self"
                                                           title="Computer Science saepe eveniet ut et volu redae ">
                                                            Computer Science saepe eveniet ut et volu redae
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

                                                        <span class="old-price product-price">$119.60 </span>&nbsp;&nbsp;
                                                        <span class="price-old">$122.00 </span>&nbsp;


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('58');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('58');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('58');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                                <div class="item ">

                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='111'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                       target="_self"
                                                       title="Compact (External Battery Power Bank) with Premium">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/12-270x270.jpg"
                                                             alt="Compact (External Battery Power Bank) with Premium">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=111"
                                                           target="_self"
                                                           title="Compact (External Battery Power Bank) with Premium ">
                                                            Compact (External Battery Power Bank) with
                                                            Premium
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$122.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('111');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('111');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('111');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                    <div class="item-wrap product-layout style1 ">
                                        <div class="product-item-container">
                                            <div class="left-block ">
                                                <div class="product-image-container so-quickview">
                                                    <a class="lt-image"
                                                       data-product='75'
                                                       href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                       target="_self"
                                                       title="Compact Portable Charger (External Battery) T21">

                                                        <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/demo/product/electronic/14-270x270.jpg"
                                                             alt="Compact Portable Charger (External Battery) T21">
                                                    </a>

                                                </div>
                                                <div class="box-label">


                                                </div>

                                            </div>


                                            <div class="right-block">
                                                <div class="caption">

                                                    <h4 class="font-ct">
                                                        <a href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/product&amp;product_id=75"
                                                           target="_self"
                                                           title="Compact Portable Charger (External Battery) T21 ">
                                                            Compact Portable Charger (External Battery) T21
                                                        </a>
                                                    </h4>


                                                    <div class="rating">
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                class="fa fa-star-o fa-stack-2x"></i></span>


                                                    </div>


                                                    <div class="content_price price font-ct">

														<span class="price product-price">
															$122.00
														</span>


                                                    </div>

                                                </div>


                                                <div class="button-group2">
                                                    <button class="bt-cart addToCart" type="button"
                                                            data-toggle="tooltip" title="Add to Cart"
                                                            onclick="cart.add('75');">
                                                        <span>Add to Cart</span></button>
                                                    <button class="bt wishlist" type="button"
                                                            data-toggle="tooltip" title="Add to Wish List"
                                                            onclick="wishlist.add('75');"><i
                                                            class="fa fa-heart"></i></button>

                                                    <button class="bt compare" type="button"
                                                            data-toggle="tooltip"
                                                            title="Compare this Product"
                                                            onclick="compare.add('75');"><i
                                                            class="fa fa-exchange"></i></button>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->


                                </div>


                            </div>
                            <!--End extraslider-inner -->

                            <script type="text/javascript">
                                //<![CDATA[
                                jQuery(document).ready(function ($) {
                                    (function (element) {
                                        var $element = $(element),
                                            $extraslider = $(".extraslider-inner", $element),
                                            _delay = 500,
                                            _duration = 800,
                                            _effect = 'none ';

                                        $extraslider.on("initialized.owl.carousel2", function () {
                                            var $item_active = $(".owl2-item.active", $element);
                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {
                                                var $item = $(".owl2-item", $element);
                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                                            }


                                            $(".owl2-controls", $element).insertBefore($extraslider);
                                            $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                                        });

                                        $extraslider.owlCarousel2({
                                            rtl: false,
                                            margin: 0,
                                            slideBy: 1,
                                            autoplay: 0,
                                            autoplayHoverPause: 0,
                                            autoplayTimeout: 0,
                                            autoplaySpeed: 1000,
                                            startPosition: 0,
                                            mouseDrag: 1,
                                            touchDrag: 1,
                                            autoWidth: false,
                                            responsive: {
                                                0: {items: 1},
                                                480: {items: 2},
                                                768: {items: 3},
                                                1200: {items: 5},
                                                1400: {items: 6}
                                            },
                                            dotClass: "owl2-dot",
                                            dotsClass: "owl2-dots",
                                            dots: false,
                                            dotsSpeed: 500,
                                            nav: false,
                                            loop: false,
                                            navSpeed: 500,
                                            navText: ["&#171 ", "&#187 "],
                                            navClass: ["owl2-prev", "owl2-next"]

                                        });

                                        $extraslider.on("translate.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            _UngetAnimate($item_active);
                                            _getAnimate($item_active);
                                        });

                                        $extraslider.on("translated.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            var $item = $(".owl2-item", $element);

                                            _UngetAnimate($item);

                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {

                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});

                                            }
                                        });

                                        function _getAnimate($el) {
                                            if (_effect == "none") return;
                                            //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                            $extraslider.removeClass("extra-animate");
                                            $el.each(function (i) {
                                                var $_el = $(this);
                                                $(this).css({
                                                    "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                    "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                    "-o-animation": _effect + " " + _duration + "ms ease both",
                                                    "animation": _effect + " " + _duration + "ms ease both",
                                                    "-webkit-animation-delay": +i * _delay + "ms",
                                                    "-moz-animation-delay": +i * _delay + "ms",
                                                    "-o-animation-delay": +i * _delay + "ms",
                                                    "animation-delay": +i * _delay + "ms",
                                                    "opacity": 1
                                                }).animate({
                                                    opacity: 1
                                                });

                                                if (i == $el.size() - 1) {
                                                    $extraslider.addClass("extra-animate");
                                                }
                                            });
                                        }

                                        function _UngetAnimate($el) {
                                            $el.each(function (i) {
                                                $(this).css({
                                                    "animation": "",
                                                    "-webkit-animation": "",
                                                    "-moz-animation": "",
                                                    "-o-animation": "",
                                                    "opacity": 1
                                                });
                                            });
                                        }

                                    })("#so_extra_slider_20522393041573891479 ");
                                });
                                //]]>
                            </script>

                        </div>

                    </div>

                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_g1nr  block">
                <div class="row">
                    <div class="banner-25 col-sm-6 banners">
                        <div>
                            <a class="bn-shadow" href="#" title="Banner 25">
                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner25.jpg"
                                     alt="Static Image">
                            </a>
                        </div>
                    </div>
                    <div class="banner-26 col-sm-6  banners">
                        <div>
                            <a class="bn-shadow" href="#" title="Banner 26">
                                <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/banner26.jpg"
                                     alt="Static Image">
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</section>
