<script src="{{asset('assets/frontend/catalog/view/javascript/so_home_slider/js/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/admin/view/template/extension/module/so_page_builder/assets/js/shortcodes.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_page_builder/js/section.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_page_builder/js/modernizr.video.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_page_builder/js/swfobject.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_page_builder/js/video_background.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_quickview/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_countdown/js/jquery.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_instagram_gallery/js/jquery.fancybox.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/so_megamenu/so_megamenu.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/isenselabs_gdpr/utils.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/isenselabs_gdpr/cookiemanager.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/catalog/view/javascript/isenselabs_gdpr/cookieconsent.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

    function _SoQuickView() {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth;
        if (windowWidth > 1200) {
            var $item_class = $('.so-quickview');
            if ($item_class.length > 0) {
                for (var i = 0; i < $item_class.length; i++) {
                    if ($($item_class[i]).find('.quickview_handler').length <= 0) {
                        var $product_id = $($item_class[i]).find('a', $(this)).attr('data-product');
                        if ($.isNumeric($product_id)) {
                            var _quickviewbutton = "<a class='btn-button btn-quickview quickview quickview_handler' href='http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=extension/soconfig/quickview&amp;product_id=" + $product_id + "' title=\"Quick View\" data-title =\"Quick View\" data-fancybox-type=\"iframe\" ><i class=\"fa fa-search\"></i></a>";
                            //$($item_class[i]).append(_quickviewbutton);
                            $($item_class[i]).find('a.lt-image').after(_quickviewbutton);
                            // if($($item_class[i]).find('a.quickview').length <= 0){

                            // }
                        }
                    }
                }
            }
        }

    }

    jQuery(document).ready(function ($) {
        _SoQuickView();
        // Hide tooltip when clicking on it
        var hasTooltip = $("[data-toggle='tooltip']").tooltip({container: 'body'});
        hasTooltip.on('click', function () {
            $(this).tooltip('hide')
        });
    });


</script>
