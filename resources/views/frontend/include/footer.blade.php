<footer class="footer-container typefooter-3">


    <div class="footer-has-toggle collapse" id="collapse-footer">
        <div class="so-page-builder">
            <section id="" class="section_1  section-color ">
                <div class="container page-builder-ltr">
                    <div class="row row_34bo  row-style ">
                        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 col_9jik  col-style">


                            <div class="module news-letter">
                                <div class="so-custom-default newsletter"
                                     style="width:100%     ; background-color: #f0f0f0 ; ">


                                    <div class="btn-group title-block">
                                        <div class="popup-title page-heading">
                                            <i class="fa fa-paper-plane-o"></i> Sign up to Newsletter
                                        </div>
                                        <div class="newsletter_promo">And receive <span>$29</span>coupon for first
                                            shopping
                                        </div>
                                    </div>
                                    <div class="modcontent block_content">
                                        <form method="post" id="signup" name="signup"
                                              class="form-group form-inline signup send-mail">
                                            <div class="input-group form-group required">
                                                <div class="input-box">
                                                    <input type="email" placeholder="Your email address..." value=""
                                                           class="form-control" id="txtemail" name="txtemail"
                                                           size="55">
                                                </div>
                                                <div class="input-group-btn subcribe">
                                                    <button class="btn btn-primary" type="submit"
                                                            onclick="return subscribe_newsletter();" name="submit">
                                                        <i class="fa fa-envelope hidden"></i>
                                                        <span>Subscribe</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>


                                    </div> <!--/.modcontent-->

                                </div>

                                <script type="text/javascript">
                                    function subscribe_newsletter() {
                                        var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                        var email = $('#txtemail').val();
                                        var d = new Date();
                                        var createdate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                        var status = 0;
                                        var dataString = 'email=' + email + '&createdate=' + createdate + '&status=' + status;

                                        if (email != "") {

                                            if (!emailpattern.test(email)) {

                                                $('.so-custom-default .show-error').remove();
                                                $('.so-custom-default .send-mail').after('<div class="alert alert-danger show-error" role="alert"> <i class="fa fa-check-circle"></i> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Invalid Email </div>')
                                                return false;
                                            } else {
                                                $.ajax({
                                                    url: 'index.php?route=extension/module/so_newletter_custom_popup/newsletter',
                                                    type: 'post',
                                                    data: dataString,
                                                    dataType: 'json',
                                                    success: function (json) {
                                                        $('.so-custom-default .show-error').remove();
                                                        console.log(json.message);
                                                        if (json.message == "Subscription Successfull") {
                                                            $('.so-custom-default .send-mail').after('<div class="alert alert-success show-error" role="alert"> <i class="fa fa-check-circle"></i> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> ' + json.message + '</div>');
                                                            setTimeout(function () {
                                                                var this_close = $('.popup-close');
                                                                this_close.parent().css('display', 'none');
                                                                this_close.parents().find('.so_newletter_custom_popup_bg').removeClass('popup_bg');
                                                            }, 3000);

                                                        } else {
                                                            $('.so-custom-default .send-mail').after('<div class="alert alert-danger show-error" role="alert"> <i class="fa fa-check-circle"></i> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> ' + json.message + '</div>');
                                                        }
                                                        var x = document.getElementsByClassName('signup');
                                                        for (i = 0; i < x.length; i++) {
                                                            x[i].reset();
                                                        }
                                                    }
                                                });
                                                return false;
                                            }
                                        } else {
                                            alert("Email Is Require");
                                            $(email).focus();
                                            return false;
                                        }
                                    }
                                </script>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col_l60i  col-style">
                            <div class="footer-social">
                                <h3 class="block-title">Follow us</h3>
                                <div class="socials">

                                    <a href="https://www.facebook.com/SmartAddons.page" class="facebook"
                                       target="_blank"><i class="fa fa-facebook"></i>
                                        <p>on</p>
                                        <span class="name-social">Facebook</span>
                                    </a>

                                    <a href="https://twitter.com/smartaddons" class="twitter" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                        <p>on</p>
                                        <span class="name-social">Twitter</span>
                                    </a>
                                    <a href="https://plus.google.com/u/0/+SmartAddons-Joomla-Magento-WordPress/posts"
                                       class="google" target="_blank">
                                        <i class="fa fa-google-plus"></i>
                                        <p>on</p>
                                        <span class="name-social">Google +</span>
                                    </a>

                                    <a href="#" class="dribbble" target="_blank"><i class="fa fa-dribbble"
                                                                                    aria-hidden="true"></i></a>

                                    <a href="#" class="instagram" target="_blank">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                        <p>on</p>
                                        <span class="name-social">Instagram</span>
                                    </a>

                                    <a href="#" class="pinterest" target="_blank"><i
                                            class="fa fa-pinterest"></i></a> <a href="#" class="linkedIn"
                                                                                target="_blank"><i
                                            class="fa fa-linkedin"></i></a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </section>
            <section id="" class="section_2  section-color ">
                <div class="container page-builder-ltr">
                    <div class="row row_m1ch  row-style  row-color ">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_itqc  col-style">
                            <div class="clearfix bonus-menus bonus-menu-4">
                                <ul>
                                    <li class="item secure col-md-3">
                                        <div class="icon">
                                        </div>
                                        <div class="text">
                                            <h5><a href="#">100% Secure Payments</a></h5>
                                            <p>All major credit & debit</p>
                                            <p> cards accepted</p>
                                        </div>
                                    </li>
                                    <li class="item help col-md-3">
                                        <div class="icon">
                                        </div>
                                        <div class="text">
                                            <h5><a href="#">Help Center</a></h5>
                                            <p>Got a question? Look no further. </p>
                                            <p> Browse our FAQs or submit your here.</p>
                                        </div>
                                    </li>
                                    <li class="item trustpay col-md-3">
                                        <div class="icon">
                                        </div>
                                        <div class="text">
                                            <h5><a href="#">TrustPay</a></h5>
                                            <p>100% Payment Protection. Easy</p>
                                            <p> Return Policy </p>
                                        </div>
                                    </li>
                                    <li class="item delivery col-md-3">
                                        <div class="icon">
                                        </div>
                                        <div class="text">
                                            <h5><a href="#">Worldwide Delivery</a></h5>
                                            <p>With sites in 5 languages, we ship to </p>
                                            <p>over 200 countries & regions.</p>
                                        </div>
                                    </li>
                                    <li class="item value col-md-3">
                                        <div class="icon">
                                        </div>
                                        <div class="text">
                                            <h5><a href="#">Great Value</a></h5>
                                            <p>We offer competitive prices on our 100</p>
                                            <p>million plus product range.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                        </div>

                    </div>
                </div>

            </section>
            <section id="" class="section_3 ">
                <div class="container page-builder-ltr">
                    <div class="row row_10tc  row-style ">
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_c22i  col-style">
                            <div class="footer-links">
                                <h4 class="title-footer">
                                    Trade Services
                                </h4>
                                <ul class="links">
                                    <li>
                                        <a href="#">Trade Assurance</a>
                                    </li>
                                    <li>
                                        <a href="#"> Business Identity</a>
                                    </li>
                                    <li>
                                        <a href="#"> Logistics Service</a>
                                    </li>
                                    <li>
                                        <a href="#"> Secure Payment</a>
                                    </li>
                                    <li>
                                        <a href="#"> Inspection Services</a>
                                    </li>
                                    <li>
                                        <a href="#"> Request for Quotation</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_hokm  col-style">
                            <div class="footer-links">
                                <h4 class="title-footer">
                                    POLICY INFO
                                </h4>
                                <ul class="links">
                                    <li>
                                        <a title="About US"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=information/information&amp;information_id=8">About
                                            US</a>
                                    </li>
                                    <li>
                                        <a title="Contact us"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=information/contact">Contact
                                            us</a>
                                    </li>
                                    <li>
                                        <a title="Warranty And Services"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=information/information&amp;information_id=5">Warranty
                                            And Services</a>
                                    </li>
                                    <li>
                                        <a title="Support 24/7 Page"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=information/information&amp;information_id=3">Support
                                            24/7 Page</a>
                                    </li>
                                    <li>
                                        <a title="Terms And Conditions"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=information/information&amp;information_id=6">Terms
                                            And Conditions</a>
                                    </li>
                                    <li>
                                        <a href="#"> CSR Policy</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_r3tw  col-style">
                            <div class="footer-links">
                                <h4 class="title-footer">
                                    Our BUSINESS
                                </h4>
                                <ul class="links">
                                    <li>
                                        <a href="#">Advertise on TopDeals</a>
                                    </li>
                                    <li>
                                        <a href="#"> Media Enquiries</a>
                                    </li>
                                    <li>
                                        <a href="#"> Be an Affiliate</a>
                                    </li>
                                    <li>
                                        <a href="#"> Deal of the Day</a>
                                    </li>
                                    <li>
                                        <a href="#"> Diwali Offers</a>
                                    </li>
                                    <li>
                                        <a href="#"> Snapdeal Gold</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_59ik  col-style">
                            <div class="footer-links">
                                <h4 class="title-footer">
                                    Our company
                                </h4>
                                <ul class="links">
                                    <li>
                                        <a title="Brands"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/manufacturer">Brands</a>
                                    </li>
                                    <li>
                                        <a title="Gift Certificates"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/voucher">Gift
                                            Certificates</a>
                                    </li>
                                    <li>
                                        <a title="Affiliates"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=affiliate/login">Affiliates</a>
                                    </li>
                                    <li>
                                        <a title="Specials"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=product/special">Specials</a>
                                    </li>
                                    <li>
                                        <a title="Returns"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/return/add">Returns</a>
                                    </li>
                                    <li>
                                        <a href="#"> Sitemap</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_xi8a  col-style">
                            <div class="footer-links">
                                <h4 class="title-footer">
                                    My Account
                                </h4>
                                <ul class="links">
                                    <li>
                                        <a title="My Account"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/account">My
                                            Account</a>
                                    </li>
                                    <li>
                                        <a title="Account Downloads"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/download">Account
                                            Downloads</a>
                                    </li>
                                    <li>
                                        <a title="Checkout"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=checkout/cart">Checkout</a>
                                    </li>
                                    <li>
                                        <a href="#"> Wishlist</a>
                                    </li>
                                    <li>
                                        <a title="Order History"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/order">Order
                                            History</a>
                                    </li>
                                    <li>
                                        <a title="Your Transactions"
                                           href="http://opencart.opencartworks.com/themes/so_topdeal3/index.php?route=account/transaction">Your
                                            Transactions</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 col_cz3o  col-style">


                            <div class="module footer-linksso-instagram-gallery-ltr home3_instagram footer-links ">

                                <h4 class="title-footer">Instargram Gallery</h4>


                                <div class="modcontent">

                                    <div class="so-instagram-gallery button-type2 7"
                                         id="instagram6247595171573891479">


                                        <div class="instagram-items-inner owl2-carousel">


                                            <div class="instagram-item">


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0LqpAz0d/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/55efb1c7e03b9ea59031536936755e14/5E68507C/t51.2885-15/e35/s320x320/17334016_1887478501539179_1959495579448901632_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/55efb1c7e03b9ea59031536936755e14/5E68507C/t51.2885-15/e35/s320x320/17334016_1887478501539179_1959495579448901632_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0K5igKga/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/c094e1b185d58ed8b2a9de2ee6ae11c0/5E4E17DE/t51.2885-15/e35/s320x320/17265674_272023933243389_3039366111326896128_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/c094e1b185d58ed8b2a9de2ee6ae11c0/5E4E17DE/t51.2885-15/e35/s320x320/17265674_272023933243389_3039366111326896128_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="instagram-item">


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0JGQACws/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/7cfd6aaf23b66a822e9b8ecce6eb24f1/5E6C567E/t51.2885-15/e35/s320x320/17494315_271111449999419_1923017606160187392_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/7cfd6aaf23b66a822e9b8ecce6eb24f1/5E6C567E/t51.2885-15/e35/s320x320/17494315_271111449999419_1923017606160187392_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0IGKg4o9/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/0342878dab5d90db5e11d0bde71df1de/5E663D43/t51.2885-15/e35/s320x320/17493562_324905107912345_7631467851978637312_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/0342878dab5d90db5e11d0bde71df1de/5E663D43/t51.2885-15/e35/s320x320/17493562_324905107912345_7631467851978637312_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="instagram-item">


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0HRag72k/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/bf191585ea916c802d5b988e00e3e5a0/5E697D8A/t51.2885-15/e35/s320x320/17493524_206552429831136_5746205933223018496_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/bf191585ea916c802d5b988e00e3e5a0/5E697D8A/t51.2885-15/e35/s320x320/17493524_206552429831136_5746205933223018496_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0F4ogF3k/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/fa0600db65c07a8890db63aa134cc3c2/5E524EDE/t51.2885-15/e35/s320x320/17438539_1912585012289034_524610343738015744_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/fa0600db65c07a8890db63aa134cc3c2/5E524EDE/t51.2885-15/e35/s320x320/17438539_1912585012289034_524610343738015744_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="instagram-item">


                                                <div class="instagram_users">
                                                    <div class="img_users">
                                                        <a title="destinoindex5"
                                                           data-href="https://www.instagram.com/p/BSA0FL2g7tS/"
                                                           class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                           rel=""
                                                           href="https://scontent.cdninstagram.com/vp/70b24ad5b2192bc9499cc833b511fae6/5E8C1C12/t51.2885-15/e35/s320x320/17439019_391188831257676_8420490534375653376_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                            <img class="image_users"
                                                                 src="https://scontent.cdninstagram.com/vp/70b24ad5b2192bc9499cc833b511fae6/5E8C1C12/t51.2885-15/e35/s320x320/17439019_391188831257676_8420490534375653376_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                                 title="destinoindex5" alt="destinoindex5"/>
                                                            <!-- <i class="fa fa-instagram"></i> -->
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="instagram_users">
                                                <div class="img_users">
                                                    <a title="destinoindex5"
                                                       data-href="https://www.instagram.com/p/BSAz8EjAeU7/"
                                                       class="instagram_gallery_image gallery_image_instagram6247595171573891479"
                                                       rel=""
                                                       href="https://scontent.cdninstagram.com/vp/2891cad30ddd4609815f999df1f0a772/5E6BF767/t51.2885-15/e35/s320x320/17437667_1994036477474793_3224405031994261504_n.jpg?_nc_ht=scontent.cdninstagram.com?taken-by=destinoindex5">
                                                        <img class="image_users"
                                                             src="https://scontent.cdninstagram.com/vp/2891cad30ddd4609815f999df1f0a772/5E6BF767/t51.2885-15/e35/s320x320/17437667_1994036477474793_3224405031994261504_n.jpg?_nc_ht=scontent.cdninstagram.com"
                                                             title="destinoindex5" alt="destinoindex5"/>
                                                        <!-- <i class="fa fa-instagram"></i> -->
                                                    </a>
                                                </div>
                                            </div>

                                        </div>


                                    </div> <!--/.instagram-items-inner-->

                                </div>
                            </div> <!-- /.modcontent-->


                            <script type="text/javascript">

                                //<![CDATA[
                                jQuery(document).ready(function ($) {
                                    ;(function (element) {
                                        var $element = $(element),
                                            $extraslider = $(".instagram-items-inner", $element),
                                            _delay = 500,
                                            _duration = 800,
                                            _effect = 'none ';

                                        this_item = $extraslider.find('div.media');
                                        this_item.find('div.item:eq(0)').addClass('head-button');
                                        this_item.find('div.item:eq(0) .media-heading').addClass('head-item');
                                        this_item.find('div.item:eq(0) .media-left').addClass('so-block');
                                        this_item.find('div.item:eq(0) .media-content').addClass('so-block');
                                        $extraslider.on("initialized.owl.carousel2", function () {
                                            var $item_active = $(".owl2-item.active", $element);
                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {
                                                var $item = $(".owl2-item", $element);
                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                                            }


                                            $(".owl2-nav", $element).insertBefore($extraslider);
                                            $(".owl2-controls", $element).insertAfter($extraslider);

                                        });

                                        $extraslider.owlCarousel2({
                                            rtl: false,
                                            margin: 15,
                                            slideBy: 1,
                                            autoplay: 0,
                                            autoplayHoverPause: 0,
                                            autoplayTimeout: 5000,
                                            autoplaySpeed: 1000,

                                            startPosition: 0,
                                            mouseDrag: 1,
                                            touchDrag: 1,
                                            autoWidth: false,
                                            responsive: {
                                                0: {items: 1},
                                                480: {items: 2},
                                                768: {items: 2},
                                                992: {items: 3},
                                                1200: {items: 3}
                                            },
                                            dotClass: "owl2-dot",
                                            dotsClass: "owl2-dots",
                                            dots: false,
                                            dotsSpeed: 500,
                                            nav: false,
                                            loop: 0,
                                            navSpeed: 500,
                                            navText: ["&#139 ", "&#155 "],
                                            navClass: ["owl2-prev", "owl2-next"]

                                        });

                                        $extraslider.on("translate.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            _UngetAnimate($item_active);
                                            _getAnimate($item_active);
                                        });

                                        $extraslider.on("translated.owl.carousel2", function (e) {


                                            var $item_active = $(".owl2-item.active", $element);
                                            var $item = $(".owl2-item", $element);

                                            _UngetAnimate($item);

                                            if ($item_active.length > 1 && _effect != "none") {
                                                _getAnimate($item_active);
                                            } else {
                                                $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                                            }
                                        });

                                        function _getAnimate($el) {
                                            if (_effect == "none") return;
                                            //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                                            $extraslider.removeClass("extra-animate");
                                            $el.each(function (i) {
                                                var $_el = $(this);
                                                $(this).css({
                                                    "-webkit-animation": _effect + " " + _duration + "ms ease both",
                                                    "-moz-animation": _effect + " " + _duration + "ms ease both",
                                                    "-o-animation": _effect + " " + _duration + "ms ease both",
                                                    "animation": _effect + " " + _duration + "ms ease both",
                                                    "-webkit-animation-delay": +i * _delay + "ms",
                                                    "-moz-animation-delay": +i * _delay + "ms",
                                                    "-o-animation-delay": +i * _delay + "ms",
                                                    "animation-delay": +i * _delay + "ms",
                                                    "opacity": 1
                                                }).animate({
                                                    opacity: 1
                                                });

                                                if (i == $el.size() - 1) {
                                                    $extraslider.addClass("extra-animate");
                                                }
                                            });
                                        }

                                        function _UngetAnimate($el) {
                                            $el.each(function (i) {
                                                $(this).css({
                                                    "animation": "",
                                                    "-webkit-animation": "",
                                                    "-moz-animation": "",
                                                    "-o-animation": "",
                                                    "opacity": 1
                                                });
                                            });
                                        }

                                    })("#instagram6247595171573891479");
                                });
                                //]]>
                            </script>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $(".gallery_image_instagram6247595171573891479").attr('rel', 'gallery').fancybox({
                                        prevMethod: false,
                                        helpers: {
                                            thumbs: {
                                                width: 50,
                                                height: 50,
                                            },

                                            title: {type: 'over'},
                                        },
                                    });
                                });
                            </script>

                        </div>

                    </div>
                </div>

            </section>
        </div>

    </div>

    <div class="footer-toggle hidden-lg hidden-md">
        <a class="showmore collapsed" data-toggle="collapse" href="#collapse-footer" aria-expanded="false"
           aria-controls="collapse-footer">
            <span class="toggle-more"><i class="fa fa-angle-double-down"></i>Show More</span>
            <span class="toggle-less"><i class="fa fa-angle-double-up"></i>Show less</span>
        </a>
    </div>


    <div class="footer-bottom ">
        <div class="container">
            <div class="row">
                <div class="col-md-7  col-sm-7 copyright">
                    So TopDeal © 2016 - 2019. All Rights Reserved. Designed by <a
                        href="http://www.opencartworks.com/" target="_blank">OpenCartWorks.Com</a>

                </div>


                <div class="col-md-5 col-sm-5 paymen">
                    <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/payment/payment.png"
                         alt="imgpayment">
                </div>


            </div>
        </div>
    </div>

</footer>
