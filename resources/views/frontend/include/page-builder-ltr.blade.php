<div class="container page-builder-ltr">
    <div class="row row_qw7j  row-style ">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col_8sot  col-style">

            <div class="module sohomepage-slider so-homeslider-ltr  ">


                <div class="modcontent">

                    <div id="sohomepage-slider1">
                        <div class="so-homeslider sohomeslider-inner-1">

                            <div class="item">
                                <a href=" #" title="Slide 6 - 1" target="_self">
                                    <img class="responsive"
                                         src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/so_home_slider/home6/slider-31-1057x490.jpg"
                                         alt="Slide 6 - 1"/>
                                </a>
                                <div class="sohomeslider-description">

                                </div>
                            </div>

                            <div class="item">
                                <a href=" #" title="Slide 6 - 2" target="_self">
                                    <img class="responsive"
                                         src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/so_home_slider/home6/slider-32-1057x490.jpg"
                                         alt="Slide 6 - 2"/>
                                </a>
                                <div class="sohomeslider-description">

                                </div>
                            </div>

                            <div class="item">
                                <a href=" #" title="Slide 6 - 3" target="_self">
                                    <img class="responsive"
                                         src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/so_home_slider/home6/slider-33-1057x490.jpg"
                                         alt="Slide 6 - 3"/>
                                </a>
                                <div class="sohomeslider-description">

                                </div>
                            </div>

                            <div class="item">
                                <a href=" #" title="Slide 6 - 4" target="_self">
                                    <img class="responsive"
                                         src="http://opencart.opencartworks.com/themes/so_topdeal3/image/cache/catalog/so_home_slider/home6/slider-34-1057x490.jpg"
                                         alt="Slide 6 - 4"/>
                                </a>
                                <div class="sohomeslider-description">

                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            var total_item = 4;
                            $(".sohomeslider-inner-1").owlCarousel2({
                                rtl: false,
                                animateOut: 'none',
                                animateIn: 'none',
                                autoplay: false,
                                autoplayTimeout: 5000,
                                autoplaySpeed: 1000,
                                smartSpeed: 500,
                                autoplayHoverPause: true,
                                startPosition: 0,
                                mouseDrag: true,
                                touchDrag: true,
                                dots: true,
                                autoWidth: false,
                                dotClass: "owl2-dot",
                                dotsClass: "owl2-dots",
                                loop: true,
                                navText: ["Next", "Prev"],
                                navClass: ["owl2-prev", "owl2-next"],

                                responsive: {
                                    0: {
                                        items: 1,
                                        nav: total_item <= 1 ? false : ((true) ? true : false),
                                    },
                                    480: {
                                        items: 1,
                                        nav: total_item <= 1 ? false : ((true) ? true : false),
                                    },
                                    768: {
                                        items: 1,
                                        nav: total_item <= 1 ? false : ((true) ? true : false),
                                    },
                                    992: {
                                        items: 1,
                                        nav: total_item <= 1 ? false : ((true) ? true : false),
                                    },
                                    1200: {
                                        items: 1,
                                        nav: total_item <= 1 ? false : ((true) ? true : false),
                                    }
                                },
                            });
                        </script>
                    </div>
                </div> <!--/.modcontent-->


            </div>


        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col_mmrl  col-style">
            <div class="banner-layout-2 h6-banner-1 clearfix">
                <div class="banner-1 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_1.jpg"
                                 alt="Static Image">
                        </a>
                    </div>
                </div>
                <div class="banner-2 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_2.jpg"
                                 alt="Static Image">
                        </a>
                    </div>

                </div>
                <div class="banner-3 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_4.jpg"
                                 alt="Static Image">
                        </a>
                    </div>
                </div>
                <div class="banner-4 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_3.jpg"
                                 alt="Static Image">
                        </a>
                    </div>

                </div>
                <div class="banner-5 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_6.jpg"
                                 alt="Static Image">
                        </a>
                    </div>
                </div>
                <div class="banner-6 banners">
                    <div>
                        <a href="#" title="Banner 2">
                            <img src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/home6_5.jpg"
                                 alt="Static Image">
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_jgpm  col-style">
            <div class="block-service-home6">
                <ul>
                    <li class="item free-ship">
                        <div class="wrap">
                            <div class="icon"></div>
                            <div class="text">
                                <h5><a href="#">Free Delivery</a></h5>
                                <p>From $59.89</p>
                            </div>
                        </div>
                    </li>
                    <li class="item support">
                        <div class="wrap">
                            <div class="icon"></div>
                            <div class="text">
                                <h5><a href="#">Support 24/7</a></h5>
                                <p>Online 24 hours</p>
                            </div>
                        </div>
                    </li>
                    <li class="item free-return">
                        <div class="wrap">
                            <div class="icon"></div>
                            <div class="text">
                                <h5><a href="#">Free return</a></h5>
                                <p>365 a day</p>
                            </div>
                        </div>
                    </li>
                    <li class="item payment">
                        <div class="wrap">
                            <div class="icon"></div>
                            <div class="text">
                                <h5><a href="#">payment method</a></h5>
                                <p>Secure payment</p>
                            </div>
                        </div>
                    </li>
                    <li class="item big-saving">
                        <div class="wrap">
                            <div class="icon"></div>
                            <div class="text">
                                <h5><a href="#">Big Saving</a></h5>
                                <p>Weeken Sales</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_stzj  block">
            <div class="cate-html">
                <ul class="cate-html-item contentslider" data-rtl="no" data-loop="no" data-autoplay="yes"
                    data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6"
                    data-margin="27" data-items_column0="6" data-items_column1="4" data-items_column2="3"
                    data-items_column3="3" data-items_column4="2" data-arrows="yes" data-pagination="no"
                    data-lazyload="yes" data-hoverpause="yes">
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/digital.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Digital</a></h4>
                        </div>
                    </li>
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/fashion.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Fashion</a></h4>
                        </div>
                    </li>
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/food.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Food & Restaurant</a></h4>
                        </div>
                    </li>
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/health.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Health Care</a></h4>
                        </div>
                    </li>
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/spa.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Spa & Massage</a></h4>
                        </div>
                    </li>
                    <li class="item">
                        <div class="item-image"><a title="Static Image" href="#"><img
                                    src="http://opencart.opencartworks.com/themes/so_topdeal3/image/catalog/demo/banners/home6/travel.png"
                                    alt="Static Image"></a></div>
                        <div class="item-content">
                            <h4><a href="#">Travel</a></h4>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

    </div>
</div>
